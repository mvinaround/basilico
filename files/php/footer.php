<footer>
	<div class="left">
		<p>Quick Links</p>
		<ul>
			<?php GetMenu(); ?>
		</ul>

	</div>
	<div class="copyright">
		<p>Content &copy; <?php echo GetAdvSetting('site_title') ?> <?php echo date("Y"); ?><br><br>Site Design & Development by <a href="http://jamieoverington.co.uk/?ref=BASILICO">Jamie Overington</a> & <a href="http://webkore.co.uk/?ref=BASILICO">Webkore CMS</a> <?php echo date("Y"); ?></p>
	</div>
</footer>
	<?php GetFacebookSDK() ?>
</body>
</html>
