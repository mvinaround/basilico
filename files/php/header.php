<?php include("admin/webkore_files/frontend-hooks.php"); ?>
<?php StoreVisitorData(); ?>
<head>
	<title><?php echo GetAdvSetting("site_title") ?></title>
	<link href="files/css/basilico.css" rel="stylesheet" />
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
	<script src="https://www.google.com/recaptcha/api.js" async defer></script>
	<script src='https://api.mapbox.com/mapbox-gl-js/v1.4.1/mapbox-gl.js'></script>
	<link href='https://api.mapbox.com/mapbox-gl-js/v1.4.1/mapbox-gl.css' rel='stylesheet' />
	<!-- Webkore CMS SEO Hooks -->
	<meta property="og:image" 		content="<?php echo GetAdvSetting('open_graph_image_url') ?>">
	<meta property="og:image:width" content="<?php echo GetAdvSetting('open_graph_image_width') ?>">
	<meta property="og:image:height" content="<?php echo GetAdvSetting('open_graph_image_height') ?>">
	<meta property="og:url"         content="<?php echo GetAdvSetting('open_graph_site_url') ?>">
	<meta property="og:type"        content="<?php echo GetAdvSetting('open_graph_site_type') ?>">
	<meta property="og:title"       content="<?php echo GetAdvSetting('site_title') ?>">
	<meta property="og:description" content="<?php echo GetAdvSetting('description') ?>">

	<meta name="theme-color" content="<?php echo GetAdvSetting('theme_colour') ?>">
	<meta name="description" content="<?php echo GetAdvSetting('description') ?>" />

	<meta name="keywords" content="<?php echo GetAdvSetting('keywords') ?>" />
	<meta name="robots" content="all" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta itemprop="name" content="<?php echo GetAdvSetting('site_title') ?>">
	<meta itemprop="url" content="<?php echo GetAdvSetting('open_graph_site_url') ?>">
</head>
<body>
