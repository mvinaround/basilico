<?php require("files/php/header.php") ?>
<?php require("files/php/menu.php") ?>
<section class="content">
		<?php echo GetSnippet("Gallery Top") ?>
		<div id="gallery-photo-mode">
			<!-- Galley Images Below -->
			<?php GetGallery(); ?>
		</div>
</section>
<?php require("files/php/footer.php") ?>