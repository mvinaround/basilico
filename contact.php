<?php require("files/php/header.php") ?>
<?php require("files/php/menu.php") ?>
<section class="content">
<?php

	$from_email ='server@basili.co.uk';
	$from_name = 'Basilico Website (WebKore CMS)';
	$to_email = GetAdvSetting('form_email');
	$to_name ='Gennaro Marzano';
	$reply_address = 'noreply@basili.co.uk';



	$site_name = GetAdvSetting('site_name');
	$show_captcha_error = false;
	$show_email_error = false;
	$email_sent = false;

	$name = $_POST["name"];
	$email = $_POST["email"];
	$phone = $_POST["phone"];
	$date = $_POST["date"];
	$time = $_POST["time"];
	$seats = $_POST["seats"];
	$other_details = $_POST["other_details"];


	if($_POST["submit"]){

			function MakeField($name, $val){
				return "<br><p><b>" . $name . ":</b></p><p>" . $val . "</p>";
			}

			$Body = "<html><body>" . "<h1>New Enquiry On " . $site_name . "</h1>" . MakeField("Name",$name) . MakeField("Reply Email",$email) . MakeField("Phone Number", $phone) . MakeField("Date", $date). MakeField("Time", $time) . MakeField("Table For", $seats) . MakeField("Other Details", $other_details) . "<br>Any problems with this email please contact <b>Jamie Overington</b>. Email <a href='mailto:mvinaround1998@gmail.com' >mvinaround1998@gmail.com</a> for support</p>";
			$Subject = "New Enquiry On " . $site_name . " From: " . $email;

			if(SendEmail($from_email,$from_name,$to_email,$to_name,$reply_address,$Body,$Subject)){
					$email_sent = true;
					$name = "";
					$email = "";
					$phone = "";
					$date = "";
					$time = "";
					$seats = "";
					$other_details = "";
			}
			else{
				$show_email_error = true;
			}
	}

?>
		<?php echo GetSnippet("Contact Page") ?>
		<?php if(GetAdvSetting("fb_enable_message_box_on_contact_page") == "on"){ ?>
			<div class="contact-top">
				<div class="contact-left">
					<?php GetFacebookMessageBox() ?>
				</div>
				<div class="contact-right">
					<div id='map'></div>
						<script>
							mapboxgl.accessToken = 'pk.eyJ1IjoibXZpbmFyb3VuZCIsImEiOiJjanJzZXozZWUwamcwNDNxczA2M2lhcTVjIn0.7LNtonmPgUfcgUrAnLh8eQ';
							var map = new mapboxgl.Map({
							container: 'map',
							center: [-2.4380103,50.714721],
							zoom: 18,
							style: 'mapbox://styles/mapbox/light-v9'
							});

							map.on('load', function() {
								map.loadImage('files/img/mapbox.png', function(error, image) {
									if (error) throw error;
									map.addImage('cat', image);
									map.addLayer({
										"id": "points",
										"type": "symbol",
										"source": {
											"type": "geojson",
											"data": {
												"type": "FeatureCollection",
												"features": [{
													"type": "Feature",
													"geometry": {
														"type": "Point",
														"coordinates": [-2.4380103,50.714721],
													}
												}]
											}
										},
										"layout": {
											"icon-image": "cat",
											"icon-size": 0.15
										}
									});
								});
							});
						</script>
				</div>
			</div>
			<br><br>
			<h1>Book A Table...</h1>
			<br>
			<p>Our online bookings will be back soon! Please email <a href="mailto:basilicodorchester@gmail.com">basilicodorchester@gmail.com</a> for any enquiries or to book a table.</p>
		<?php } ?>
		<!--
		<form class="form" action="" method="POST">
			<?php if($email_sent){?>
				<div class="form-sent">
					<p>Awesome! Your request has been sent. We will get back to you as soon as possible.</p>
				</div>
			<?php } ?>

			<?php if($show_captcha_error){?>
				<div class="form-error">
					<p>Please verify that you are a human. This is used to prevent spam.</p>
				</div>
			<?php } ?>

			<?php if($show_email_error){?>
				<div class="form-error">
					<p>The request failed to send, please try again. If the problem persists, please email or call us directly.</p>
				</div>
			<?php } ?>

			<p class="required" style="text-align:center;">* Fields are required.</p>
			<div class="input-container">
				<label for="fullname">Full Name <span class="required">*</span></label>
				<input type="text" id="fullname" name="name" value="<?php echo $name ;?>" required/>
			</div>
			<div class="input-container">
				<label for="email">Email Address <span class="required">*</span></label>
				<input type="email" id="email" name="email" value="<?php echo $email ;?>" required/>
			</div>
			<div class="input-container">
				<label for="phone">Phone Number</label>
				<input type="text" id="phone" name="phone" value="<?php echo $phone ;?>"/>
			</div>
			<div class="input-container">
				<label for="date">Date</label>
				<input type="date" id="date" name="date" value="<?php echo $date ;?>"/>
			</div>
			<div class="input-container">
				<label for="time">Table for what time</label>
				<input type="time" id="time" name="time" value="<?php echo $time ;?>"/>
			</div>
			<div class="input-container">
				<label for="seats">Table for how many people</label>
				<input type="number" step="1" min="1" max="50" id="seats" name="seats" value="<?php echo $seats ;?>"/>
			</div>
			<div class="input-container">
				<label for="other">Any other details</label>
				<textarea id="other" name="other_details"/><?php echo $other_details ;?></textarea>
			</div>

			<div class="input-container">
				<label for="other">&nbsp;</label>
  				<input type="submit" name="submit" value="Send">
			</div>
		</form>
	-->

</section>
<?php require("files/php/footer.php") ?>
