<?php include("webkore_files/osc_core.php");
	LoadHeader();
	LoadMenu();
?>
<section>


	<h1>User Logs</h1>
	<p>All activity that happens within the system gets logged here.</p>

	<div class="list">
		<table>
			<tr><th>ID</th><th>Username</th><th>IP</th><th>Actions</th><th>Timestamp</th></tr>
<?php
				$result = SQLQuery("SELECT * FROM user_logs ORDER BY date_created DESC");

				if(mysqli_num_rows($result) > 0){

			    	while($row = mysqli_fetch_array($result)){
			    		?>
			    		<tr>
			    			<td><?php echo $row['id'] ?></td>
			    			<td><?php echo $row['username'] ?></td>
			    			<td><?php echo $row['ip'] ?></td>
			    			<td><?php echo $row['action'] ?></td>
			    			<td><?php echo $row['date_created'] ?></td>
			    		</tr>

			    		<?php
			   		}
			   	}
			   	else{
			    	echo "<tr><td><p>No Logs Found.</p></td></tr>";
				}
		?>
		</table>
	</div>
</section>


<?php LoadFooter(); ?>
