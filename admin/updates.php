<?php include("webkore_files/osc_core.php");
	LoadHeader();
	LoadMenu();
	CheckForUser();
	
?>
<section>

	<h1>WebKore Updates!</h1>

	<div class="wk-update">
		<h2>Update 2.3.3 - 19/02/19</h2>
		<h3>Site Performance & Dashboard Update</h3>
		<p>The site performance page has been fixed and updated to use MapBox instead of Google Maps. This both saves money and fixes a problem with the map not rendering that's been happening on and off for a few months now.</p>
		<p>The dashboard has a couple of new features, this includes what booking you have coming up (if enabled) and a quick few stats that refresh live on people visiting the site!</p>

		
			<p class="wk-published"><strong>Published By:</strong> Jamie Overington - <a href="mailto:mvinaround1998@gmail.com?subject:WebKore User Message">mvinaround1998@gmail.com</a><img src="webkore_files/img/jamie.png" class="round-img"/></p>
	</div>


	<div class="wk-update">
		<h2>Update 2.2.5 - 04/02/19</h2>
		<h3>Visual changes</h3>
		<p>Just some visual 'eye candy' as such. A redesign of the login and password reset pages, a future look into what webkore might look like in the near future. Update 2.2.0 was very stable hence the long time between updates... and why this is only visual. More awesome features in the pipeline!</p>
		<p>Also I have removed the custom themes for the time being, I think with the overhaul of the visual side of the system coming soon the idea of custom themes would prove to be more work than needed.</p>

		
			<p class="wk-published"><strong>Published By:</strong> Jamie Overington - <a href="mailto:mvinaround1998@gmail.com?subject:WebKore User Message">mvinaround1998@gmail.com</a><img src="webkore_files/img/jamie.png" class="round-img"/></p>
	</div>

	<div class="wk-update">
		<h2>Update 2.2.0 - 13/08/18</h2>
		<h3>Custom Content Pages</h3>
		<p>The latest site I have worked on required fully custom content. So we now have that ability! Content pages now generate links if you have the latest content loader working. If you're interested please email me and I can incorperate it into any current site</p>

		
			<p class="wk-published"><strong>Published By:</strong> Jamie Overington - <a href="mailto:mvinaround1998@gmail.com?subject:WebKore User Message">mvinaround1998@gmail.com</a><img src="webkore_files/img/jamie.png" class="round-img"/></p>
	</div>

	<div class="wk-update">
		<h2>Update 2.1.5 - 10/06/18</h2>
		<h3>Quick Mobile &amp; Tablet Fixes</h3>
		<p>Fixed display issues on smaller laptops, tablets and ipads.</p>

		
			<p class="wk-published"><strong>Published By:</strong> Jamie Overington - <a href="mailto:mvinaround1998@gmail.com?subject:WebKore User Message">mvinaround1998@gmail.com</a><img src="webkore_files/img/jamie.png" class="round-img"/></p>
	</div>

	<div class="wk-update">
		<h2>Update 2.1.0 - 17/01/18</h2>
		<h3>Social Media Features</h3>
		<p>Facebook like buttons are now placeable in any content or snippet area.</p>
		<p>Facebook message form to directly contact you via your facebook page now available.</p>
		

		<h3>Security Updates</h3>
		<p>Password storage and login checking now only one way. (Encrypt only!). So even if the database was to be hacked, no passwords would be leaked.</p>
		<p>To tie in with the new password hashing, there's a new password reset link and email system on the login screen.</p>

		<h3>UI Tweaks</h3>
		<p>Added Dynamic menus, change your nav bar items...</p>
		<p>Added cookie controlled toggle menus, that now stay collapsed after clicking.</p>

		
			<p class="wk-published"><strong>Published By:</strong> Jamie Overington - <a href="mailto:mvinaround1998@gmail.com?subject:WebKore User Message">mvinaround1998@gmail.com</a><img src="webkore_files/img/jamie.png" class="round-img"/></p>
	</div>
	<div class="wk-update">
		<h2>Update 2.0.3 - 17/01/18</h2>
		<h3>Security Hole (Major!)</h3>
		<p>Fixed a major security hole around server redirect hacks enabling access.</p>
		
			<p class="wk-published"><strong>Published By:</strong> Jamie Overington - <a href="mailto:mvinaround1998@gmail.com?subject:WebKore User Message">mvinaround1998@gmail.com</a><img src="webkore_files/img/jamie.png" class="round-img"/></p>
	</div>

	<div class="wk-update">
		<h2>Update 2.0.2 - 14/09/17</h2>
		<h3>Beta Themes</h3>
		<p>Added 2 Beta Themes to enable to Reskin WebKore, A JJMedia themed one (Green) and a GTR themed one (Cream?)</p>
		<p>To request a custom theme, just ask me, email or facebook</p>
		
			<p class="wk-published"><strong>Published By:</strong> Jamie Overington - <a href="mailto:mvinaround1998@gmail.com?subject:WebKore User Message">mvinaround1998@gmail.com</a><img src="webkore_files/img/jamie.png" class="round-img"/></p>
	</div>

	<h1>WebKore Updates!</h1>
	<div class="wk-update">
		<h2>Update 2.0.1 - 03/09/17</h2>
		<h3>Inventory item listing</h3>
		<p>Added a section to log items to a list for inventory checking.</p>
		<p>General WebKore work. New features to come.</p>
		
			<p class="wk-published"><strong>Published By:</strong> Jamie Overington - <a href="mailto:mvinaround1998@gmail.com?subject:WebKore User Message">mvinaround1998@gmail.com</a><img src="webkore_files/img/jamie.png" class="round-img"/></p>
	</div>


	<div class="wk-update">
		<h2>Update 2.0.0 - 27/03/17</h2>
		<h3>Whats New in 2.0.0!</h3>
		<p>Nothing that a user can use/see</p>
		<p>A LOT of database code has changed, Every feature in WebKore has been adjusted to the new PHP standard of MySQLi...</p>
		<p>PLEASE if you do find something is broken, message me via text or facebook as soon as you notice it, Not that anything /shoud/ be broken, but just as a precaution.</p>
		<p>Added Services section for future websites or website rebuilds.</p>
		
			<p class="wk-published"><strong>Published By:</strong> Jamie Overington - <a href="mailto:mvinaround1998@gmail.com?subject:WebKore User Message">mvinaround1998@gmail.com</a><img src="webkore_files/img/jamie.png" class="round-img"/></p>
	</div>

	<div class="wk-update">
		<h2>Update 1.9.5 - 20/02/17</h2>
		<h3>Whats New?</h3>
		<p>Fixed calendar months not showing correct amount of days in a month.</p>
		<p>Fixed email logs so emails can be read. (Both PHP & SQL Errors fixed.)</p>

		<p>Not done too much in the progress of WebKore (due to personal problems)... More Soon!</p>
		
			<p class="wk-published"><strong>Published By:</strong> Jamie Overington - <a href="mailto:mvinaround1998@gmail.com?subject:WebKore User Message">mvinaround1998@gmail.com</a><img src="webkore_files/img/jamie.png" class="round-img"/></p>
	</div>

	<h1>WebKore Updates!</h1>
	<div class="wk-update">
		<h2>Update 1.9.4 - 19/11/16</h2>
		<h3>Whats New?</h3>
		<p>Fixed a typo on all websites! Updated the Geo-Map to a more "Up to date" and more accurate looking map that groups data and clusters it (spreads out on zoom in)</p>
		
			<p class="wk-published"><strong>Published By:</strong> Jamie Overington - <a href="mailto:mvinaround1998@gmail.com?subject:WebKore User Message">mvinaround1998@gmail.com</a><img src="webkore_files/img/jamie.png" class="round-img"/></p>
	</div>

	<div class="wk-update">
		<h2>Update 1.9.3 - 17/09/16</h2>
		<h3>Whats New?</h3>
		<p>Quick update to fix the booking system. Fixed holiday, you can now mark dates off as holiday! Also bookings no longer require a total or deposit amount to be entered... (As per request)</p>
		
			<p class="wk-published"><strong>Published By:</strong> Jamie Overington - <a href="mailto:mvinaround1998@gmail.com?subject:WebKore User Message">mvinaround1998@gmail.com</a><img src="webkore_files/img/jamie.png" class="round-img"/></p>
	</div>
	
	<div class="wk-update">
		<h2>Update 1.9.2 - 01/08/16</h2>
		<h3>Whats New?</h3>
		<p>This is a hotfix to tighten up security around the whole system. This is due to multiple attempts by a few hackers trying to find a way inside... We are however extremely secure and no un-authorised accesss has been granted OR any data accessed. No need to worry!</p>

		<p>The system has also had some menu changes to make the system easier to use on both mobile and desktop. Some extra second menus for editing and managing stuff added to allow future additions to be implimented easier.		
		</p>

		<h3>Bug Fixes:</h3>
		<ul>
		<li>Locked down image uploads, double checks that the user is logged in.</li>
		<li>Failed login attempts now get logged to the system.</li>
		<li>hacker attempts also logged into the system.</li>
		<li>Various new features added, not going live at the current time...</li>
		</ul>
		
			<p class="wk-published"><strong>Published By:</strong> Jamie Overington - <a href="mailto:mvinaround1998@gmail.com?subject:WebKore User Message">mvinaround1998@gmail.com</a><img src="webkore_files/img/jamie.png" class="round-img"/></p>
	</div>

	<div class="wk-update">
		<h2>Update 1.9.1 - 01/07/16</h2>
		<h3>Whats New?</h3>
		<p>This update makes the majority of the system mobile friendly! Yeah IKR, taken its time :P</p>
		<p>A Few data-sorting bugs fixed. Purely displaying data, nothing to do with storing or writing data (so its all safe)</p>

		<p>Most of the panels are all ready to use... On mobile devices. Although its <strong>vertical only</strong>.</p>
		<p>Tables of data might still be buggy. I am still in the process of displaying the data in a clean way for a small screen...</p>

		<h3>Bug Fixes:</h3>
		<ul>
		<li>Dates displaying incorrectly on Booking list.</li>
		</ul>

		<h3>Important Notices:</h3>
		<p style="color: #0d0">The email issue from the last update is not a system bug, more of an error with hotmail... Any issues please do email me.</p>

			<p class="wk-published"><strong>Published By:</strong> Jamie Overington - <a href="mailto:mvinaround1998@gmail.com?subject:WebKore User Message">mvinaround1998@gmail.com</a><img src="webkore_files/img/jamie.png" class="round-img"/></p>
	</div>

	<div class="wk-update">
		<h2>Update 1.8.5 - 04/04/16</h2>
		<h3>Whats New?</h3>
		<p>This update is more of a patch than anything, it does however include a few new features...</p>

		<p>There is now a System Logs > Emails section that logs all emails going through the system, used more as a backup just incase you don't receive an email for whatever reason...</p>
		<p>There is also a System Logs > Users section. This logs ALL user activity within the system, it mainly logs any changes to data and files but also logs users logging in and out of the system, along with their computers IP address.</p>

		<h3>Bug Fixes:</h3>
		<ul>
		<li>Bookings can now be deleted (if you have them enabled on your admin)</li>
		<li>Fixed various text across pages to make more sense</li>
		<li>Fixed bookings page AGAIN (if enabled) to now show the correct date eg saturday the 31st will no longer say saturday the 6th...</li>
		<li>Fixed a domain control issue with emails being sent out.</li>
		</ul>

		<h3>Important Notices:</h3>
		<p style="color: #d00">Currently emails going to Hotmail/Outlook addresses seem to not be received by the email they were sent to. This is a problem currently out of my control and I am working along side Mailgun (Who manage the email sending) to solve this issue.</p>

			<p class="wk-published"><strong>Published By:</strong> Jamie Overington - <a href="mailto:mvinaround1998@gmail.com?subject:WebKore User Message">mvinaround1998@gmail.com</a><img src="webkore_files/img/jamie.png" class="round-img"/></p>
	</div>


	<div class="wk-update">
		<h2>Update 1.8.0 - 28/03/16</h2>
		<h3>So What's new?</h3>
		<p>To start off with we have a new page that you're looking at right now! A log for all the updates to the system...</p>
		<p>
		Other new features include a new <strong>Image Uploader</strong> for any content editor box on the sytem.<br>
		<strong>So what does all that mean? </strong><br>
		Well that means that when you go to edit content on your site with the little Microsoft Word like boxes you can now upload images directly onto the server to store them and use them on your pages! Its not the neatest way of doing it, but it seems like the most logical solution at the current time (Im sure it will be replaced in the near future).
		</p>

		<p>
		Another new feature is the dashboard! Well its always been there but with nothing on it... If anyone has any suggestions on what to add or include on that please let me know!
		</p>

		<p>
		Sites that have customizable banners now have a section in their admins that they can upload new banners onto (if you require a template for your site please email me, if you would like banners created for your site to fit from images you already have, then this can be done for you at a small fee.)
		</p>

		<h3>Bug Fixes:</h3>
		<ul>
			<li>Fixed issue with booking management not displaying the correct date on the list view.</li>
			<li>Reduced duplicate data being shown on the Google Geo Chart on site performance page.</li>
			<li>Improved system stability with changing the way database management works.</li>
			<li>Added module backbone for future "custom" admin panel sections.</li>
		</ul>

		<p>Thats all for now, more updates coming soon!</p>




		<p class="wk-published"><strong>Published By:</strong> Jamie Overington - <a href="mailto:mvinaround1998@gmail.com?subject:WebKore User Message">mvinaround1998@gmail.com</a><img src="webkore_files/img/jamie.png" class="round-img"/></p>
	</div>
</section>


<?php LoadFooter(); ?>
