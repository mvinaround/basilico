<?php include("webkore_files/osc_core.php");
	LoadHeader();
	LoadMenu();
	CheckForUser();

	$alert_box = false;
	$alert_text = "";
	$alert_type = "";

	if($_POST["action"] == "generate_new_desktop_key"){

		$key = GenerateNewDesktopKey();
		$alert_box = true;
		$alert_text = "Generated Key.";
		$alert_type = "success";

		LogAction("Generated New Software Key: " . $key);


	}

	if($_POST["action"] == "update_basic_settings"){

		UpdateSetting("main_site_url");
		UpdateSetting("site_title");
		UpdateSetting("site_header_line");
		UpdateSetting("site_sub_header_line");
		UpdateSetting("landline");
		UpdateSetting("mobile");
		UpdateSetting("email");
		UpdateSetting("form_email");

		$alert_box = true;
		$alert_text = "Updated Basic Settings";
		$alert_type = "success";

		LogAction("Updated Basic Settings");


	}

	if($_POST["action"] == "update_meta_settings"){

		UpdateSetting("open_graph_image_url");
		UpdateSetting("open_graph_image_width");
		UpdateSetting("open_graph_image_height");
		UpdateSetting("open_graph_site_url");
		UpdateSetting("open_graph_site_type");
		UpdateSetting("open_graph_title");

		UpdateSetting("description");
		UpdateSetting("keywords");
		UpdateSetting("theme_colour");


		$alert_box = true;
		$alert_text = "Updated Meta Tags &  SEO Settings";
		$alert_type = "success";

		LogAction("Updated Meta/SEO Settings");


	}

	if($_POST["action"] == "update_code_generator_snippets"){
		UpdateSetting("menu_link_line");
	}



	
?>
<section>
	<h1>Advanced Settings</h1>
	<p>Listed Here are all the settings used across the site. Please be careful when changing these settings as they can effect your site performace or break some things. For help please email <a href="mailto:contact@jjmediauk.co.uk?subject=Advanced Settings Help">contact@jjmediauk.co.uk</a> for more help.</p>
	<?php
		//Alert Box:
		if($alert_box == true){
			echo '<div class="alert ' . $alert_type .'">' . $alert_text . '</div>';
		}
	?>
	<div class="advanced-settings">
		<h2>Basic Settings</h2>
		<form for="advanced.php" method="POST">
		<?php
			FormField( "Main Site URL", "text", 0, "get-setting","Main Site URL, Used for Password Reset emails.");
			FormField( "Site Title", "text", 0, "get-setting","Shown in tab at the top of a browser, and in google searches");
			FormField( "Site Header Line", "text", 0, "get-setting","Shown on ALL pages at the top (only if you dont have a logo)");
			FormField( "Site Sub Header Line", "text", 0, "get-setting","Shown on ALL pages at the top below header line (only if you dont have a logo)");
			FormField( "Landline", "tel", 0, "get-setting","Your landline phone number shown on your website.");
			FormField( "Mobile", "tel", 0, "get-setting","Your mobile phone number shown on your website.");
			FormField( "Email", "email", 0, "get-setting","Your email address shown on your website.");
			FormField( "Form Email", "email", 0, "get-setting","Where emails get sent to from any contact form on the site.");

		?>
			<div class="form-field">
				<input type="hidden" name="action" value="update_basic_settings" />
				<input type="submit" value="Update Basic Settings" />
			</div>
		</form>
	</div>

	<div class="meta-tags">
		<h2>Meta Tags &amp; SEO</h2>
		<form for="advanced.php" method="POST">
			<?php
				FormField( "Open Graph Image URL", "url", 0, "get-setting","Passed to facebook when sharing the site.");
				FormField( "Open Graph Image Width", "text", 0, "get-setting","");
				FormField( "Open Graph Image height", "text", 0, "get-setting","");
				FormField( "Open Graph Site URL", "url", 0, "get-setting","Site link when shared to Facebook");
				FormField( "Open Graph Site Type", "text", 0, "get-setting","if unsure, put as article");
				FormField( "Open Graph Title", "text", 0, "get-setting","Title shown on facebook when sharing");
			?>
			<div class="form-field">
				<label for="open_graph_description">Open Graph Description &amp; For Google</label>	
				<textarea name="description"><?php echo GetSetting('description') ?></textarea>
			</div>
			<div class="form-field">
				<label for="keywords">Keywords</label>
				<input type="text" name="keywords" value="<?php echo GetSetting('keywords') ?>" data-role="tagsinput"/>

			</div>
			<?php
				FormField( "Theme Colour","text",0, "get-setting","include #")
			?>

			<div class="form-field">
				<input type="hidden" name="action" value="update_meta_settings" />
				<input type="submit" value="Update Meta & Seo Settings" />
			</div>

		</form>
	</div>

	<div class="advanced-settings">
		<h2>Code Generator Snippets</h2>
		<form for="advanced.php" method="POST">
		<?php

			FormField( "Menu Link Line", "text", 0, "get-setting","Short bit of code that gets wrapped around dynamic menu links. put {LINK} to insert the <a> tag");

		?>
			<div class="form-field">
				<input type="hidden" name="action" value="update_code_generator_snippets" />
				<input type="submit" value="Update Code Generator Settings" />
			</div>
		</form>
	</div>

	<div class="desktop">
		<h2>Desktop Software Connection Key:</h2>
		<?php FormField("Desktop Software Key","text",0,"get-setting","Desktop Software Login Key") ?>
		
		<form for="advanced.php" method="POST">
			<div class="form-field">
				<input type="hidden" name="action" value="generate_new_desktop_key" />
				<input type="submit" name="newkeyplz" value="Generate New Key" />
			</div>
		</form>
	</div>

	<div class="dashboard-quicklinks">
		<h2>Dashboard Elements Visible</h2>
		<h3 class="option-header">Visitor Data (Beta):</h3>
		<?php SwitchFeature("ql-visits");?>

		<h3 class="option-header">Next Upcoming Booking (Beta):</h3>
		<?php SwitchFeature("ql-next-booking");?>

		<br><br>
		<h2>QuickLinks Shown On Main Dashboard</h2>
		<h3 class="option-header">Testimonials:</h3>
		<?php SwitchFeature("ql-testimonials");?>

		<h3 class="option-header">Bookings:</h3>
		<?php SwitchFeature("ql-bookings");?>

		<h3 class="option-header">Gallery:</h3>
		<?php SwitchFeature("ql-gallery");?>

		<h3 class="option-header">Site Performance:</h3>
		<?php SwitchFeature("ql-site-performance");?>

		<h3 class="option-header">Banners</h3>
		<?php SwitchFeature("ql-banners");?>
		
		<h3 class="option-header">Snippets:</h3>
		<?php SwitchFeature("ql-snippets");?>

		<h3 class="option-header">Services:</h3>
		<?php SwitchFeature("ql-services");?>

		<h3 class="option-header">Contents:</h3>
		<?php SwitchFeature("ql-contents");?>

		<h3 class="option-header">Dynamic Menus:</h3>
		<?php SwitchFeature("ql-dynamic-menus");?>

		<h3 class="option-header">Videos:</h3>
		<?php SwitchFeature("ql-videos");?>

		<h3 class="option-header">Inventory:</h3>
		<?php SwitchFeature("ql-inventory");?>

		<h3 class="option-header">Updates (Recommended On):</h3>
		<?php SwitchFeature("ql-updates");?>
	</div>
	<br><br>

</section>
<script>

var APIKey = "<?php echo $enc_key ?>"

$(".onoffswitch-checkbox").change(function() {
	var val = 0;
	var name = this.id.replace("f_","")

    if(this.checked) {
    	val = 1
    }

    FeatureToggle(name,val,APIKey);
});


</script>

<?php LoadFooter(); ?>
