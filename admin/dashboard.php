<?php include("webkore_files/osc_core.php");
	LoadHeader();
	LoadMenu();
	CheckForUser();
	
?>
<section>
	<h1>Dashboard</h1>
	<h2>Welcome Back <?php echo GetUsername() ?></h2>
	<br><br>
	<?php if(GetFeature(1,0,"ql-next-booking")){

		$date = SQLQuery("SELECT * FROM bookings WHERE booking_date > NOW() AND holiday = 0 ORDER BY booking_date ASC LIMIT 1");

		if(mysqli_num_rows($date) > 0){

			$data = mysqli_fetch_array($date);


	?>
	<div class="dash-next-booking">
		<h3>Next Upcoming Booking</h3>
		<ul>	
			<li><span class="mini-title">Date: </span><?php echo MakeUKDate($data["booking_date"]) ?></li>
			<li><span class="mini-title">Times From: </span><?php echo $data["time_from"] ?> <span class="mini-title">to</span> <?php echo $data["time_to"] ?></li>
			<li><span class="mini-title">Client: </span><?php echo $data["name"] ?></li>
		</ul>
		<a class="btn btn-info" href="bookings.php?action=view&id=<?php echo $data["id"] ?>">View Booking</a>
	</div>

	<?php
		}
		else{

			?>
			<div class="dash-next-booking">
			<h3>Next Upcoming Booking</h3>
			<p class="no-booking">No Upcoming Bookings :(</p>
			</div>

			<?php
		}
	}

	 ?>


	<?php if(GetFeature(1,0,"ql-visits")){ ?>
	<div class="dash-quick-stats">
		<h3>Website Quick Stats from <span id="qs-from"></span> to <span id="qs-to"></span></h3>
		<p id="qs-refresh">Loading...</p>

		<div class="quick-data-square">
			<p class="text">Unique Visitors</p>
			<p class="value" id="qs-u-visitors">0</p>
		</div>
		<div class="quick-data-square">
			<p class="text">Unique Views</p>
			<p class="value" id="qs-u-views">0</p>
		</div>
		<div class="quick-data-square">
			<p class="text">Unique Page Views</p>
			<p class="value" id="qs-p-views">0</p>
		</div>
		<div class="quick-data-square">
			<p class="text">Different Browsers</p>
			<p class="value" id="qs-browsers">0</p>
		</div>
	</div>

	<script>DashQuickStats();</script>

	<?php } ?>


	<div class="quick-buttons">
		<?php if(GetFeature(1,0,"ql-snippets")){ ?>
			<a href="snippets.php" class="dash-button">
				<i class="fa fa-code"></i>
				<p>Snippets</p>
			</a>
		<?php } ?>

		<?php if(GetFeature(1,0,"ql-services")){ ?>
			<a href="services.php" class="dash-button">
				<i class="fa fa-space-shuttle"></i>
				<p>Services</p>
			</a>
		<?php } ?>

		<?php if(GetFeature(1,0,"ql-contents")){ ?>
		<a href="contents.php" class="dash-button">
			<i class="fa fa-file-text"></i>
			<p>Pages</p>
		</a>
		<?php } ?>

		<?php if(GetFeature(1,0,"ql-testimonials")){ ?>
			<?php if(GetFeature(1,0,"testimonials")){ ?>
			<a href="testimonials.php" class="dash-button">
				<i class="fa fa-comments"></i>
				<p>Testimonials</p>
			</a>
			<?php } ?>
		<?php } ?>

		<?php if(GetFeature(1,0,"ql-dynamic-menus")){ ?>
			<?php if(GetFeature(1,0,"dynamic-menus")){ ?>
			<a href="menus.php" class="dash-button">
				<i class="fa fa-bars"></i>
				<p>Dynamic Menus</p>
			</a>
			<?php } ?>
		<?php } ?>

		<?php if(GetFeature(1,0,"ql-inventory")){ ?>
			<?php if(GetFeature(1,0,"inventory")){ ?>
			<a href="inventory.php" class="dash-button">
				<i class="fa fa-shopping-basket"></i>
				<p>Inventory</p>
			</a>
			<?php } ?>
		<?php } ?>

		<?php if(GetFeature(1,0,"ql-gallery")){ ?>
			<?php if(GetFeature(1,0,"gallery")){ ?>
				<a href="gallery.php" class="dash-button">
					<i class="fa fa-picture-o"></i>
					<p>Gallery</p>
				</a>
			<?php } ?>
		<?php } ?>

		<?php if(GetFeature(1,0,"ql-banners")){ ?>
			<?php if(GetFeature(1,0,"banners")){ ?>
				<a href="banners.php" class="dash-button">
					<i class="fa fa-picture-o"></i>
					<p>Banners</p>
				</a>
			<?php } ?>
		<?php } ?>

		<?php if(GetFeature(1,0,"ql-videos")){ ?>
			<?php if(GetFeature(1,0,"videos")){ ?>
				<a href="videos.php" class="dash-button">
					<i class="fa fa-youtube-play"></i>
					<p>Videos</p>
				</a>
			<?php } ?>
		<?php } ?>

		<?php if(GetFeature(1,0,"ql-updates")){ ?>
			<a href="updates.php" class="dash-button">
				<i class="fa fa-thumbs-up"></i>
				<p>Updates</p>
			</a>
		<?php } ?>
	</div>
	
</section>


<?php LoadFooter(); ?>
