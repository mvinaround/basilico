<?php include("webkore_files/osc_core.php");
	LoadHeader();
	LoadMenu();

	if($_GET["holiday"] == "YeahBaby"){
		if(SQLQuery("INSERT INTO bookings (holiday, booking_date) VALUES ('1','" . $_GET["date"] . "')" )){
			LogAction("Created Holiday [" . $booking_date . "]");
			header("location:calendar.php?alert=true&alert_text=Holiday%20Added&alert_type=success");
		}
	}

	if($_GET["holiday"] == "HowAboutNo"){
		if(SQLQuery("DELETE FROM bookings WHERE holiday = 1 AND booking_date = '" . $_GET["date"] . "'" )){
			LogAction("Removed Holiday [" . $booking_date . "]");
			header("location:calendar.php?alert=true&alert_text=Holiday%20Removed&alert_type=success");
		}
	}
	if($_GET["holiday"] == "RemoveHoliday"){
		if(SQLQuery("DELETE FROM bookings WHERE holiday = 1 AND id = '" . $_GET["id"] . "'" )){
			LogAction("Removed Holiday [" . $booking_date . "]");
			header("location:bookings.php?alert=true&alert_text=Holiday%20Removed&alert_type=success");
		}
	}

	
	$alert_box = false;
	$alert_text = "";
	$alert_type = "";

	//On adding a new one:
	if($_POST["action"] == "doadd"){
		$holiday = SQLCheckbox($_POST["holiday"]);
		$booking_date = $_POST["booking_date"];
		$name = $_POST["name"];
		$address = $_POST["address"];
		$landline = $_POST["landline"];
		$mobile = $_POST["mobile"];
		$email = $_POST["email"];
		$deposit = $_POST["deposit"];
		$total = $_POST["total"];
		$deposit_paid = SQLCheckbox($_POST["deposit_paid"]);
		$total_paid = SQLCheckbox($_POST["total_paid"]);
		$$payment_notes = $_POST["payment_notes"];
		$time_from = $_POST["time_from"];
		$time_to = $_POST["time_to"];
		$notes = $_POST["notes"];
		$venue = $_POST["venue"];


		if(SQLQuery("INSERT INTO bookings (holiday, booking_date, name, address, landline, mobile, email, deposit, deposit_paid, total, total_paid, payment_notes, time_from, time_to, notes, venue) VALUES ('" . $holiday . "','" . $booking_date . "','" . SQLWrap($name) . "','" . SQLWrap($address) . "','" . SQLWrap($landline) . "','" . SQLWrap($mobile) . "','" . SQLWrap($email) . "','" . $deposit . "','" . $deposit_paid . "','" . $total . "','" . $total_paid . "','" . SQLWrap($payment_notes) . "','" . $time_from . "','" . $time_to . "','" . SQLWrap($notes) . "','" . SQLWrap($venue) . "')" )){
			$alert_box = true;
			$alert_text = "Added New Booking!";
			$alert_type = "success";
			LogAction("Created A New Booking [" . $booking_date . "]");
		}
		else{
			$alert_box = true;
			$alert_text = "Failed To Save Booking.";
			$alert_type = "danger";
		}

	}
	//On updating an existing one:

	elseif($_POST["action"] == "doedit"){
		
		if(SQLQuery('UPDATE bookings SET booking_date="' . $_POST["booking_date"] . '", name="' . SQLWrap($_POST["name"]) . '",address="' . SQLWrap($_POST["address"]) . '",landline="' . SQLWrap($_POST["landline"]) . '",mobile="' . SQLWrap($_POST["mobile"]) . '",email="' . SQLWrap($_POST["email"]) . '",deposit="' . $_POST["deposit"] . '",deposit_paid="' . SQLCheckbox($_POST["deposit_paid"]) . '",total="' . $_POST["total"] . '",total_paid="' . SQLCheckbox($_POST["total_paid"]) . '",payment_notes="' . SQLWrap($_POST["payment_notes"]) . '",time_from="' . $_POST["time_from"] . '",time_to="' . $_POST["time_to"] . '",notes="' . SQLWrap($_POST["notes"]) . '",venue="' . SQLWrap($_POST["venue"]) . '" WHERE id=' . $_POST["id"])){
			$alert_box = true;
			$alert_text = "Updated Booking.";
			$alert_type = "success";
			LogAction("Updated Booking ID: #" . $_POST["id"] . "<br> Dated: " . $_POST["booking_date"]);	
		}
		else{
			$alert_box = true;
			$alert_text = "Failed To Update Booking.";
			$alert_type = "danger";
		}
	}

	else{
		$action = $_GET["action"];	
	}
?>
<section>


	<h1>Bookings</h1>
	<?php
		//Alert Box:
		if($alert_box == true){
			echo '<div class="alert ' . $alert_type .'">' . $alert_text . '</div>';
		}
	?>

	<?php 

		if($action == "add" or $action == "edit"){
			$name = "";
			$description = "";
			$content = "";
			$send_action = "doadd";
			$id = 0;
			$button_text = "Create";

			if($action == "edit"){
				$id = $_GET["id"];

				$query = mysqli_fetch_array(SQLQuery("SELECT * FROM bookings WHERE id = " . intval($id) . " LIMIT 1"));

				$send_action = "doedit";
				$button_text = "Update";

				$holiday = SQLCheckbox($_POST["holiday"]);
				$booking_date = $query["booking_date"];
				$name = $query["name"];
				$address = $query["address"];
				$landline = $query["landline"];
				$mobile = $query["mobile"];
				$email = $query["email"];
				$deposit = $query["deposit"];
				$total = $query["total"];
				$payment_notes = $query["payment_notes"];
				$time_from = $query["time_from"];
				$time_to = $query["time_to"];
				$notes = $query["notes"];
				$venue = $query["venue"];


				if($query["total_paid"] == 1 ){
					$total_paid = "checked";
				}

				if($query["deposit_paid"] == 1 ){
					$deposit_paid = "checked";
				}

			}

			if($action == "add"){
				$booking_date = $_GET["date"];
			}




	?>

	<div class="add">
		<form method="post">
			<h3>Booking Details</h3>
			<?php
				FormField("Booking Date","date", 1, $booking_date,"Format for date is: DD/MM/YYYY");
				FormField("Time From","time", 0, $time_from,"");
				FormField("Time To","time", 0, $time_to,"");
				FormTextarea("Notes", 0, $notes);
			?>
			<br>
			<h3>Client Info</h3>
			<?php
				FormField("Name", "text", 1, $name, "John Doe");
				FormTextarea("Address", 0, $address);
				FormField("Landline", "text", 0, $landline, "01935 123456");
				FormField("Mobile", "text", 0, $mobile, "07527 852741");
				FormField("Email", "email", 0, $email, "example@example.com");
			?>
			<br>
			<h3>Payment Info</h3>
			<div class="form-field">
				<label for="total_paid">Total Paid?</label>
				<input type="checkbox" name="total_paid" <?php echo $total_paid ?> />
			</div>
			<div class="form-field">
				<label for="total">Total Price (Including Any Deposit)</label>
				<input type="text" pattern="^\\$?(([1-9](\\d*|\\d{0,2}(,\\d{3})*))|0)(\\.\\d{1,2})?£" name="total"  value="<?php echo $total ?>">
			</div>
			<div class="form-field">
				<label for="deposit_paid">Deposit Paid?</label>
				<input type="checkbox" name="deposit_paid" <?php echo $deposit_paid ?> />
			</div>
			<div class="form-field">
				<label for="deposit">Deposit (If Required/Taken)</label>
				<input type="text" pattern="^\\$?(([1-9](\\d*|\\d{0,2}(,\\d{3})*))|0)(\\.\\d{1,2})?£" name="deposit"  value="<?php echo $deposit ?>">
			</div>
			<?php
				FormTextarea("Payment Notes", 0, $payment_notes);
			?>
			<h3>Venue Details</h3>
			<?php
				FormTextarea("Venue",0, $venue)
			?>
			<input type="hidden" name="action" value="<?php echo $send_action;?>" />
			<input type="hidden" name="id" value="<?php echo $id;?>" />
			<div class="form-field">
				<input type="submit" value="<?php echo $button_text ?> Booking" />
			</div>
		</form>
	</div>


<?php
		}

		if($action == "view"){
			$query = SQLQuery('SELECT * FROM bookings WHERE id = ' . $_GET["id"]);
			$data = mysqli_fetch_array($query)


	?>
		<h2>Booking #<?php echo $data["id"] ?></h2>
		<br>
		<h3>Actions:<br><a class="btn btn-delete" onclick="DBDelete(<?php echo $_GET['id'] ?>,'bookings')">Delete</a><a class="btn btn-add" href="bookings.php?action=edit&id=<?php echo $_GET['id']?>">Edit Booking</a></h3>
		<br>
		<p><span class="mini-title">Date</span><?php echo MakeUKDate($data["booking_date"]) ?></p>
		<p><span class="mini-title">Times</span><?php echo $data["time_from"] ?> to <?php echo $data["time_to"] ?></p>
		<p id="notes"><span class="mini-title">Notes</span><?php echo $data["notes"] ?></p>
		
		<h2>Client Details</h2>
		<br>
		<p><span class="mini-title">Name</span><?php echo $data["name"] ?></p>
		<p><span class="mini-title">Email</span><a class="link" href="mailto:<?php echo $data['email'] ?>"><?php echo $data["email"] ?></a></p>
		<p><span class="mini-title">Landline</span><a class="link" href="mailto:<?php echo $data['landline'] ?>"><?php echo $data["landline"] ?></a></p>
		<p><span class="mini-title">Mobile</span><a class="link" href="mailto:<?php echo $data['mobile'] ?>"><?php echo $data["mobile"] ?></a></p>
		<p id="address"><span class="mini-title">Address</span><?php echo $data["address"] ?></p>
		
		<h2>Payment Details</h2>
		<br>
		<p><span class="mini-title">Total</span>&pound;<?php echo $data["total"] ?> - <?php echo Paid($data["total_paid"]) ?></p>
		<?php if($data["deposit"]){ ?>
		<p><span class="mini-title">Deposit</span>&pound;<?php echo $data["deposit"] ?> - <?php echo Paid($data["deposit_paid"]) ?></p>
		<?php } ?>
		<p><span class="mini-title">Payment Notes</span><?php echo $data["payment_notes"] ?></p>

		<h2>Venue Details</h2>
		<br>
		<p id="venue"><span class="mini-title">Venue</span><?php echo $data["venue"] ?></p>

		<script>
			ProcessBookingData();
		</script>


	<?php



		}

		if($action == ""){

	?>
	<div class="cal-controls year-picker">
		<button id="year-back"><i class="fa fa-chevron-circle-left" ></i></button>
		<p id="selected-year"></p>
		<button class="right" id="year-forward" ><i class="fa fa-chevron-circle-right"></i></button>
	</div>

	<p><a href="bookings.php?action=add" class="btn btn-add"><i class="fa fa-plus"></i>  Create New Booking</a></p>

	<div id="calendar" class="list">
	</div>

	<script src="webkore_files/webkore_bookings.js"></script>


	<?php } ?>
</section>


<?php LoadFooter(); ?>
