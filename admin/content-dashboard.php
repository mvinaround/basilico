<?php include("webkore_files/osc_core.php");
	LoadHeader();
	LoadMenu();
	CheckForUser();
	
?>
<section>
	<h1>Editable Content</h1>
	<br><br>
	<div class="quick-buttons">
		<a href="snippets.php" class="dash-button">
			<i class="fa fa-code"></i>
			<p>Snippets</p>
		</a>

		<a href="contents.php" class="dash-button">
			<i class="fa fa-file-text"></i>
			<p>Pages</p>
		</a>

		<?php if(GetFeature(1,0,"dynamic-menus")){ ?>
			<a href="menus.php" class="dash-button">
				<i class="fa fa-bars"></i>
				<p>Dynamic Menu</p>
			</a>
		<?php } ?>

		<?php if(GetFeature(1,0,"testimonials")){ ?>
			<a href="testimonials.php" class="dash-button">
				<i class="fa fa-comments"></i>
				<p>Testimonials</p>
			</a>
		<?php } ?>

		<?php if(GetFeature(1,0,"services")){ ?>
			<a href="services.php" class="dash-button">
				<i class="fa fa-space-shuttle"></i>
				<p>Services</p>
			</a>
		<?php } ?>

		<?php if(GetFeature(1,0,"gallery")){ ?>
			<a href="gallery.php" class="dash-button">
				<i class="fa fa-picture-o"></i>
				<p>Gallery</p>
			</a>
		<?php } ?>

		<?php if(GetFeature(1,0,"banners")){ ?>
			<a href="banners.php" class="dash-button">
				<i class="fa fa-picture-o"></i>
				<p>Banners</p>
			</a>
		<?php } ?>

		<?php if(GetFeature(1,0,"videos")){ ?>
			<a href="videos.php" class="dash-button">
				<i class="fa fa-youtube-play"></i>
				<p>Videos</p>
			</a>
		<?php } ?>

		<?php if(GetFeature(1,0,"inventory")){ ?>
			<a href="inventory.php" class="dash-button">
				<i class="fa fa-shopping-basket"></i>
				<p>Inventory</p>
			</a>
		<?php } ?>


	</div>
</section>


<?php LoadFooter(); ?>
