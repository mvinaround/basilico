<?php include("webkore_files/osc_core.php");
	LoadHeader();
	CheckForUserLoginScreen();

	if($_POST["action"] == "reset-send"){
		if(empty($_POST["email"]) == 0){
			GenerateResetPasswordEmail($_POST["email"]);	
		}
	}

	if($_POST["action"] == "reset-password"){
		ResetPassword($_POST["email"],$_POST["password"],$_POST["key"]);
	}
	
?>
<div id="bgp"></div>
<div class="osc-logo"></div>
<div class="login-screen">
<?php if($_GET["view"] == "reset"){ ?>

	<h3>Forgotten Password</h3>
	<BR>
	<p>Please enter your email address below. If there is an associated account, you will receive a reset link in your inbox.</p>
	<br>
	<form method="post" action="forgot-password.php?view=email-sent">
		<div class="g-form-field">
			<input type="email" name="email" value="<?php echo $email; ?>" requried placeholder="Email">
		</div>
		<div class="g-form-field">
			<input type="submit" value="Reset Password" requried>
			<input type="hidden" name="action" value="reset-send">
		</div>
	</form>


	<?php
	}
	elseif($_GET["view"] == "change-password"){
	 ?>
	<h2>Set Your New Password</h2>
	<p>Please enter your email address below along with your new password.</p>
	 <form method="post" action="forgot-password.php">
		<div class="g-form-field">
			<input type="email" name="email" requried placeholder="Email">
		</div>
		<div class="g-form-field">
			<input type="password" name="password" requried placeholder="New Password">
		</div>
		<div class="g-form-field">
			<input type="submit" value="Reset Password" requried>
			<input type="hidden" name="action" value="reset-password">
			<input type="hidden" name="key" value="<?php echo $_GET['key'] ?>">
		</div>
	</form>


	 <?php }
	 elseif($_GET["view"] == "email-sent"){ ?>
	 <h2>Email Sent</h2>

	 <?php
	 }
	 else{
	 ?>
	 <h2>All Done...</h2>
	 <br>
	 <form method="GET" action="index.php">
	 	<div class="g-form-field">
			<input type="submit" value="Log In">
		</div>
	</form>
	 <?php
	 }
	 ?>
	<br>
</div>

<script src="webkore_files/lib/particles.min.js"></script>
<script>
particlesJS.load('bgp', 'webkore_files/lib/pjsconfig.json');
</script>



