<?php include("webkore_files/osc_core.php");
	LoadHeader();
	LoadMenu();

	
	$alert_box = false;
	$alert_text = "";
	$alert_type = "";

?>

<section>

	<h1>Calendar</h1>
	<div class="cal-controls year-picker">
		<button id="year-back"><i class="fa fa-chevron-circle-left" ></i></button>
		<p id="selected-year"></p>
		<button class="right" id="year-forward" ><i class="fa fa-chevron-circle-right"></i></button>
	</div>


	<?php
		//Alert Box:
		if($alert_box == true){
			echo '<div class="alert ' . $alert_type .'">' . $alert_text . '</div>';
		}
	?>
	<div id="calendar"></div>
	<div class="options-box">
		<h3>Loading...</h3>

	</div>
	<div class="options-background"></div>
</section>

<script src="webkore_files/osc_js_calendar.js"></script>


<?php LoadFooter(); ?>
