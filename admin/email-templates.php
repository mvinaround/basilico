<?php include("webkore_files/osc_core.php");
	LoadHeader();
	LoadMenu();

	
	$alert_box = false;
	$alert_text = "";
	$alert_type = "";

	//On adding a new one:
	if($_POST["action"] == "doadd"){
		$name = $_POST["name"];
		$head = $_POST["head"]; 
		$body = $_POST["body"];
		$subject = $_POST["subject"];

		if(SQLQuery("INSERT INTO email_templates (name,head,body,subject) VALUES ('" . mysqli_real_escape_string($name) . "','" . mysqli_real_escape_string($head) . "','" . mysqli_real_escape_string($body) . "','" . mysqli_real_escape_string($subject) . "')" )){
			$alert_box = true;
			$alert_text = "Created new email template named: " . $name;
			$alert_type = "success";

			LogAction("Created a new Email Template: [" . $name . "]");
		}
		else{
			$alert_box = true;
			$alert_text = "Failed To Save Email Template.";
			$alert_type = "danger";
		}

	}
	//On updating an existing one:

	elseif($_POST["action"] == "doedit"){
		if(SQLQuery("UPDATE email_templates SET name = '" . mysqli_real_escape_string($_POST["name"]) . "', head = '" . mysqli_real_escape_string($_POST["head"]) . "', body = '" . mysqli_real_escape_string($_POST["body"]) . "', subject = '" .  mysqli_real_escape_string($_POST["subject"]) . "' WHERE id = ". $_POST["id"])){

			$alert_box = true;
			$alert_text = "Updated ". $_POST["name"] . " Template!";
			$alert_type = "success";

			LogAction("Updated Email Template #" . $_POST["id"] . ": [" . $_POST["name"] . "]");
		}
		else{
			$alert_box = true;
			$alert_text = "Failed To Update Template.";
			$alert_type = "danger";
		}
	}

	else{
		$action = $_GET["action"];	
	}
?>
<section>


	<h1>Email Templates</h1>
	<p>Here you can edit and design the emails you would like to batch send to clients, You can edit the look and the content and change the subject line.</p>
	<?php
		//Alert Box:
		if($alert_box == true){
			echo '<div class="alert ' . $alert_type .'">' . $alert_text . '</div>';
		}
	?>

	<?php 
		if($action == "add" or $action == "edit"){
			$name = "";
			$body = "";
			$head = "";
			$subject = "";
			$send_action = "doadd";
			$id = 0;
			$button_text = "Create";

			if($action == "edit"){
				$id = $_GET["id"];
				$query = mysqli_fetch_array(SQLQuery("SELECT * FROM email_templates WHERE id = " . intval($id) . " LIMIT 1"));
				$name = $query["name"];
				$body = $query["body"];
				$head = $query["head"];
				$subject = $query["subject"];
				$send_action = "doedit";
				$button_text = "Update";	

			}


	?>

	<div class="add">
		<form method="post">
			<div class="form-field">
				<label for="name">Template Name</label>
				<input type="text" name="name" value="<?php echo $name;?>" required/>
			</div>
			<div class="form-field">
				<label for="subject">Subject</label>
				<input type="text" name="subject" value="<?php echo $subject;?>" required/>
			</div>
			<div class="form-field">
				<label for="head">Head (Meta Data &amp; Other Hidden Technical Data)</label>
				<textarea name="head" ><?php echo $head;?></textarea>
			</div>
			<div class="form-field">
				<label for="body">Body (Email Content)</label>
				<div class="alert info">
					<ul>
						<li>To insert a clients name use {CLIENT_NAME}</li>
						<li>To insert a clients email address use {CLIENT_EMAIL}</li>
						<li>Bare in mind, these emails will be sent to multiple clients, so by using the auto fill (in the above 2 points) it will automatically add the details.</li>
					</ul>
				</div>
				<textarea id="htmleditor" name="body"><?php echo $body;?></textarea>
				<?php MakeEditor("#htmleditor",500); ?>
			</div>
			<input type="hidden" name="action" value="<?php echo $send_action;?>" />
			<input type="hidden" name="id" value="<?php echo $id;?>" />
			<div class="form-field">
				<input type="submit" value="<?php echo $button_text ?> Template" />
			</div>
		</form>
	</div>


	<?php
		}
		if($action == ""){

	?>
	<div class="list">
		<a href="email-templates.php?action=add" class="btn btn-add"><i class="fa fa-plus"></i>  Create New Email Template</a>
		<table>
			<tr><th>Name</th><th>Actions</th></tr>
<?php
				$result = SQLQuery("SELECT * FROM email_templates ");

				if(mysqli_num_rows($result) > 0){

			    	while($row = mysqli_fetch_array($result)){
			    		?>
			    		<tr id="email_templates-<?php echo $row['id'] ?>" >
			    			<td><?php echo $row['name'] ?></td>
			    			<td class="table-actions">
			    				<a class="btn btn-add" href="email-templates.php?action=edit&id=<?php echo $row["id"] ?>">Edit</a>
			    				<a class="btn btn-delete" onclick="DBDelete(<?php echo $row['id'] ?>,'email_templates')">Delete</a>

			    			</td>
			    		</tr>

			    		<?php
			   		}
			   	}
			   	else{
			    	echo "<tr><td><p>No Templates Found.</p></td></tr>";
				}
		?>
		</table>
	</div>

	<?php } ?>
</section>


<?php LoadFooter(); ?>
