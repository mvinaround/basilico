<?php include("webkore_files/osc_core.php");
	LoadHeader();
	LoadMenu();

	
	$alert_box = false;
	$alert_text = "";
	$alert_type = "";

	//On adding a new one:
	if($_POST["action"] == "doadd"){
		if(SQLQuery("INSERT INTO users (name,email,password,enabled) VALUES ('" . $_POST["name"] . "','" . $_POST["email"] . "','" . HashPassword(SQLSafe($_POST["password"])) . "'," . SQLCheckbox($_POST["enabled"]) . ")" )){
			$alert_box = true;
			$alert_text = "Added New User!";
			$alert_type = "success";
			LogAction("Created a new User: [" .$_POST["name"] . "]");
		}
		else{
			$alert_box = true;
			$alert_text = "Failed To Save User.";
			$alert_type = "danger";
		}

	}
	//On updating an existing one:

	elseif($_POST["action"] == "doedit"){
		

		if($_POST["password"] == ""){
			//NO Password Changed:
			if(SQLQuery("UPDATE users SET name = '" . $_POST["name"] . "', email = '" . $_POST["email"] . "', enabled = " . SQLCheckbox($_POST["enabled"]) . " WHERE id = ". $_POST["id"])){
				$alert_box = true;
				$alert_text = "Updated ". $_POST["name"] . " User!";
				$alert_type = "success";
				LogAction("Updated User: [" . $_POST["name"] . "]");	
			}
			else{
				$alert_box = true;
				$alert_text = "Failed To Update User.";
				$alert_type = "danger";
			}
		}
		else{
			//Password has been changed:
			if(SQLQuery("UPDATE users SET name = '" . $_POST["name"] . "', email = '" . $_POST["email"] . "', password = '" . HashPassword($_POST["password"]) . "', enabled = " . SQLCheckbox($_POST["enabled"]) . " WHERE id = ". $_POST["id"])){
				$alert_box = true;
				$alert_text = "Updated ". $_POST["name"] . " User!";
				$alert_type = "success";
				LogAction("Updated User: [" . $_POST["name"] . "]");	
			}
			else{
				$alert_box = true;
				$alert_text = "Failed To Update User.";
				$alert_type = "danger";
			}
		}

		
	}

	else{
		$action = $_GET["action"];	
	}
?>
<section>


	<h1>Users</h1>
	<p>Manage system users here.</p>
	<?php
		//Alert Box:
		if($alert_box == true){
			echo '<div class="alert ' . $alert_type .'">' . $alert_text . '</div>';
		}
	?>

	<?php 
		if($action == "add" or $action == "edit"){
			$name = "";
			$email = "";
			$enabled = "";
			$send_action = "doadd";
			$id = 0;
			$password_required = 1;
			$button_text = "Create";

			if($action == "edit"){
				$id = $_GET["id"];
				$password_required = 0;

				$query = mysqli_fetch_array(SQLQuery("SELECT * FROM users WHERE id = " . intval($id) . " LIMIT 1"));
				$name = $query["name"];
				$email = $query["email"];
				$enabled = $query["enabled"];
				
				$send_action = "doedit";
				$button_text = "Update";	

			}


	?>

	<div class="add">
		<form method="post">
			<?php
			FormField("Name","text",1,$name,"John Doe");
			FormField("Email","email",1,$email,"john@mail.com - Used For Login");
			FormField("Password","password",0,"","");
			?>
			<div class="form-field">
				<label for="enabled">Account Enabled</label>
				<input type="checkbox" name="enabled" <?php echo FormCheckbox($enabled); ?>/>
			</div>
			<input type="hidden" name="action" value="<?php echo $send_action;?>" />
			<input type="hidden" name="id" value="<?php echo $id;?>" />
			<div class="form-field">
				<input type="submit" value="<?php echo $button_text ?> User" />
			</div>
		</form>
	</div>


	<?php
		}
		if($action == ""){

	?>
	<div class="list">
		<a href="users.php?action=add" class="btn btn-add"><i class="fa fa-plus"></i>  Create New User</a>
		<table>
			<tr><th>Users' Name</th><th>Email Address</th><th>Enabled</th><th>Actions</th></tr>
<?php
				$result = SQLQuery("SELECT * FROM users");

				if(mysqli_num_rows($result) > 0){

			    	while($row = mysqli_fetch_array($result)){
			    		?>
			    		<tr id="users-<?php echo $row['id'] ?>" >
			    			<td><?php echo $row['name'] ?></td>
			    			<td><?php echo $row['email'] ?></td>
			    			<td><?php TickCross($row['enabled']); ?></td>
			    			<td class="table-actions">
			    				<?php if($row['admin'] == 0){ ?>
			    				<a class="btn btn-add" href="users.php?action=edit&id=<?php echo $row["id"] ?>">Edit</a>
			    				<a class="btn btn-delete" onclick="DBDelete(<?php echo $row['id'] ?>,'users')">Delete</a>
			    				<?php } ?>
			    			</td>
			    		</tr>

			    		<?php
			   		}
			   	}
			   	else{
			    	echo "<tr><td><p>No Testimonials Found.</p></td></tr>";
				}
		?>
		</table>
	</div>

	<?php } ?>
</section>


<?php LoadFooter(); ?>
