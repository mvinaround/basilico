/*
 __          __  _     _  __                     _  _____    _____               
 \ \        / / | |   | |/ /                    | |/ ____|  / ____|              
  \ \  /\  / /__| |__ | ' / ___  _ __ ___       | | (___   | |     ___  _ __ ___ 
   \ \/  \/ / _ \ '_ \|  < / _ \| '__/ _ \  _   | |\___ \  | |    / _ \| '__/ _ \
    \  /\  /  __/ |_) | . \ (_) | | |  __/ | |__| |____) | | |___| (_) | | |  __/
     \/  \/ \___|_.__/|_|\_\___/|_|  \___|  \____/|_____/   \_____\___/|_|  \___|
                                                                                 
                                                                                 
===========================================================================================================================

Created By: Jamie Overington
Copyright: 2016
Created 01/02/2016
Last Updated: 24/02/2016

===[ Requirements ]========================================
 - Requires JQuery 2+ (Whole script)
 - Requires Moment.js (For calendar use)
===========================================================================================================================
*/
var APILink = "webkore_files/json_api.php"

// 0 Closed, 1 Open
var EmailScriptUsed = 0;
var MenuState = 0;


function NyanNyanNyan(cat){
	if(cat == "cat"){
		$("#WebKore").addClass("NyanCat")
		console.log("%cNyan Cat <3","font-size: 100px;")
	}
}

function ImageManager(){
	var strippedPathname = window.location.pathname
	strippedPathname = strippedPathname.replace("contents.php", "")
	strippedPathname = strippedPathname.replace("snippets.php", "")
	strippedPathname = strippedPathname.replace("services.php", "")
	window.open("http://" + window.location.host + strippedPathname + "image_uploads.php", "Image Manager", "height=800,width=500");
}


function DBDelete(obj_id,obj_type){
	if(confirm("Are you sure you want to delete this?")){
		$.getJSON(APILink,{
			action: "delete",
			object: obj_type,
			id: obj_id
		})
		.done(function(data){
			console.log(data)
			if(data[1] == 1){
				$("#" + obj_type + "-" + obj_id).animate({'line-height':'0px','height':'0px','opacity':0},500).hide(1);
				if(obj_type == "bookings"){
					window.location.href = "bookings.php";
				}
			}

		})
	}
}

function ImageDelete(obj_id){
	if(confirm("Are you sure you want to delete this image?")){
		$.getJSON(APILink,{
			action: "delete-image",
			file: $("#image-" + obj_id).attr("data-fs-url")
		})
		.done(function(data){
			if(data[1] == 1){
			$("#image-" + obj_id).animate({'opacity':0},500).hide(1);
			}

		})
	}

}

function ImageGetLink(image_id){
	image = $("#image-" + image_id).attr("data-fs-url")
	link = "http://" + window.location.hostname + "/admin/" + image

	$("#copy-link-box").attr("value",link);
	var tbdd = $(".textbox-dropdown")

	tbdd.removeClass("tb-dd-hide")
	tbdd.addClass("tb-dd-show")
}

function GotLink(){
	$("#copy-link-box").attr("value","");
	var tbdd = $(".textbox-dropdown")

	tbdd.removeClass("tb-dd-show")
	tbdd.addClass("tb-dd-hide")	
}

function FeatureToggle(feature_name,val,key){
	$.getJSON(APILink,{
			action: "feature",
			name: feature_name,
			key: key,
			val: val

		})
		.done(function(data){
			if(data[1] == 1){
			console.log("WebKore CMS [superuser]: Updated " + feature_name + " To the value of: " + val)
			}

	})
}

$(window).resize(function(){
	var width = $('.g-image').width()
	$('.g-image').css("height", width + "px")
	$('.g-image').children("a").css({"line-height": width + "px" , "font-size": (width/5) + "px"})
})

$(window).on("orientationchange",function(){
	if($(document).width() > $(document).height()){
		$(".mobile-vertical-only").show();
		console.log("Wrong Rotation")
	}
	else{
		$(".mobile-vertical-only").hide();
		console.log("Correct Rotation")
	}
})

$(document).ready(function(){
	var width = $('.g-image').width()
	$('.g-image').css("height", width + "px")
	$('.g-image').children("a").css({"line-height": width + "px" , "font-size": (width/5) + "px"})

	$(".hide-button").hide();
})

function ToggleMobileMenu(){
	if(MenuState == 0){
		$(".menubar").removeClass("mobile-menubar-closed")
		$(".osc-logo-mb").addClass("open-menu")
		MenuState = 1;
	}
	else{
		$(".menubar").addClass("mobile-menubar-closed")
		$(".osc-logo-mb").removeClass("open-menu")
		MenuState = 0;
	}
}


function CleanYoutubeURL(){
	ChangeEmbedStatus("Checking...","rgb(50,50,50)")
	var code = $("#embed_code").val()

	ChangeEmbedStatus("Cleaning...","rgb(50,50,50)")
	var cleaned = code.replace("https://youtu.be/","")

	cleaned = cleaned.replace('<iframe width="560" height="315" src="https://www.youtube.com/embed/',"")
	cleaned	= cleaned.replace('" frameborder="0" allowfullscreen></iframe>',"")
	cleaned	= cleaned.replace("https://www.youtube.com/watch?v=","")
	cleaned	= cleaned.replace("http://www.youtube.com/watch?v=","")
	cleaned	= cleaned.replace("https://youtu.be/","")

	$("#embed_code").val(cleaned)

	if(cleaned.length < 12){
		ChangeEmbedStatus("Cleaned. URL Correct, Preview Updated","rgb(0,140,0)")
		$("#yt_preview").attr("src","https://www.youtube.com/embed/" + cleaned)
		$("#yt_thumb").attr("value", "http://img.youtube.com/vi/" + cleaned + "/maxresdefault.jpg")
	}
	else{
		ChangeEmbedStatus("Please paste a correct link... (Or click here to retry)","rgb(140,0,0)")
		$("#yt_preview").attr("src","")
		$("#yt_thumb").attr("value", "")
	}
}

function ChangeEmbedStatus(text,colour){
	$("#embed_status").html(text).css("color",colour)
}

function SendEmail(){
	if(EmailScriptUsed == 0){
		EmailScriptUsed = 1;

		//Find Selected Template...
		


	}
	else{
		console.log("Script Used. Please Try Again Later.")
	}

}


function GenerateFacebookLikePreview(){
	$("#fb-like-preview").html("<iframe></iframe>");


	layout = $("#fb_like_layout").find(":selected").val()
	action = $("#fb_like_action_type").find(":selected").val()
	size = $("#fb_like_button_size").find(":selected").val()
	colour = $("#fb_like_button_colour").find(":selected").val()
	url = $("#fb_page_url").val()

	console.log("Layout:", $("#fb_like_layout").find(":selected").val())
	console.log("Button Text:", $("#fb_like_action_type").find(":selected").val())
	console.log("Size:", $("#fb_like_button_size").find(":selected").val())
	

	string = 'https://www.facebook.com/v2.7/plugins/like.php?action=' + action + '&app_id=181687705935449&channel=http%3A%2F%2Fstaticxx.facebook.com%2Fconnect%2Fxd_arbiter%2Fr%2FlY4eZXm_YWu.js%3Fversion%3D42%23cb%3Df8f2445c5a8814%26domain%3Dlocalhost%26origin%3Dhttp%253A%252F%252Flocalhost%252Ff112aed3676c6d8%26relation%3Dparent.parent&container_width=1904&href=' + url + '&colourscheme=' + colour + '&layout=' + layout + '&locale=en_GB&sdk=joey&share=true&show_faces=true&size=' + size;

	$("#fb-like-preview").html("<iframe id='ifb' src='" + string + "''></iframe>");


	


}

function ProcessNewlines(item){
	replace = $(item).html().replace(/(\r\n|\n|\r)/g,"<br />");

	$(item).html(replace)
}

function ProcessBookingData(){
	ProcessNewlines("#address");
	ProcessNewlines("#venue");
	ProcessNewlines("#notes");
}

function DashQuickStats(){
	FetchQuickStats()
	count = 15;
	setInterval(function(){
		if(count < 1){
			FetchQuickStats()
			$("#qs-refresh").html("Refreshing...");
			count = 15;
		}
		else{
			$("#qs-refresh").html("Refreshing in " + count + "...");
			count -=1;
			
		}

	},1000)
	
}

function FetchQuickStats(){
	$.getJSON(APILink,{
			action: "get_quick_stats",
		})
		.done(function(data){
			console.log(data)
			console.log(data[0])
			$("#qs-from").html(moment(data[1]).format("D [of] MMM"))
			$("#qs-to").html(moment(data[2]).format("D [of] MMM"))
			$("#qs-u-visitors").html(data[3])
			$("#qs-u-views").html(data[4])
			$("#qs-p-views").html(data[5])
			$("#qs-browsers").html(data[6])


		})

}