/*
 __          __  _     _  __                 _____      _                _            
 \ \        / / | |   | |/ /                / ____|    | |              | |           
  \ \  /\  / /__| |__ | ' / ___  _ __ ___  | |     __ _| | ___ _ __   __| | __ _ _ __ 
   \ \/  \/ / _ \ '_ \|  < / _ \| '__/ _ \ | |    / _` | |/ _ \ '_ \ / _` |/ _` | '__|
    \  /\  /  __/ |_) | . \ (_) | | |  __/ | |___| (_| | |  __/ | | | (_| | (_| | |   
     \/  \/ \___|_.__/|_|\_\___/|_|  \___|  \_____\__,_|_|\___|_| |_|\__,_|\__,_|_|   
                                                                                      
                                                                                      
===========================================================================================================================

Created By: Jamie Overington
Copyright: 2016
Created 01/02/2016
Update 1: 26/02/2016
Update 2: 20/02/2017 - Fixed days

===[ Requirements ]========================================
 - Requires JQuery 2+ (Whole script)
 - Requires Moment.js (For calendar use)
 - Requires osc_js_core.js
 - Requires the WebKore API for data hook ins.
===========================================================================================================================
*/

$("footer").ready(function(){
	LoadDefaults();

})

var defaultMonth 
var defaultYear  
var viewType
var defaultContainer = $("#calendar") 

function LoadDefaults(){
	defaultMonth = moment().format('MM')
	defaultYear  = moment().format('YYYY')
	viewType	 = "year"

	LoadYear(defaultYear,defaultContainer);
	console.log("WebKore CMS [calendar]: Loaded Defaults");
	$("#selected-year").html(defaultYear)

	//Click hook-ins:
	$("#year-back").click(function(){
		YearSelector(0)
	})

	$("#year-forward").click(function(){
		YearSelector(1)
	})


	//Hide Options Box
	$(".options-box").addClass("op-hidden").addClass("op-fade").html("")
	$(".options-background").addClass("op-hidden").addClass("op-fade")

}
// UI ========================================

function YearSelector(val){
	var currentYear = parseInt($("#selected-year").html())
	if(val == 0){
		currentYear -= 1
		$("#selected-year").html(currentYear)
		LoadYear(currentYear, defaultContainer)
	}
	else{
		currentYear += 1
		$("#selected-year").html(currentYear)
		LoadYear(currentYear, defaultContainer)

	}
}

function DayClicked(day){
	date = (day["currentTarget"]["id"])
	year = $("#selected-year").html()

	writeDate = moment(year + "-" + date).format("dddd D [of] MMMM YYYY")

	URLDate = moment(year + "-" + date).format("YYYY-MM-DD")
	//Show Box

	var header   = "<h3>Create Booking?</h3>"
	var text     = "<p>Would you like to create a booking for " + writeDate + "?</p>"
	var extraText = ""
	
	var button1  = '<a class="btn btn-add" href="bookings.php?action=add&date=' + URLDate + '" >Create A New Booking</a>'
	var button2  = '<a class="btn btn-holiday" href="bookings.php?holiday=YeahBaby&date=' + URLDate + '" >Mark As Holiday</a>'
	var button3  = '<a class="btn btn-info" id="cancel" >Cancel</a>'

	if($("#" + date).hasClass("holiday")){
		button2 = '<a class="btn btn-holiday" href="bookings.php?holiday=HowAboutNo&date=' + URLDate + '" >Remove Holiday</a>'
		extraText = "<p><strong>Note:</strong> This date is currently marked as Holiday.</p>"
	}


	var buttons  = '<p class="butttttons">' + button1 + button2 + button3 + "</p>"

	$(".options-background").removeClass("op-hidden")
	$(".options-box").removeClass("op-hidden").html(header + text + extraText + buttons)

	$("#cancel").click(function(){
		HideOptions();
	})

	setTimeout(function(){
		$(".options-box").removeClass("op-fade")
		$(".options-background").removeClass("op-fade")
	},50)


}

function HideOptions(){
	$(".options-box").addClass("op-fade")
	$(".options-background").addClass("op-fade")

	setTimeout(function(){
		$(".options-box").addClass("op-hidden").html("")
		$(".options-background").addClass("op-hidden")
	},450)
}

// ===========================================

function LoadYear(y,container){
	i = 1
	defaultContainer.html("")
	while(i < 13){
		fill_table(i, y , container)
		i ++
	}
	MarkDates(y)
}

function MarkDates(y){
	var APILink = "webkore_files/json_api.php"
	$.getJSON(APILink,{
			"action": "cal",
			"c-action":"load_year",
			"c-year": y
		})
		.done(function(data){
			console.log("WebKore CMS [calendar]: Fetched Dates For " + y)
			$.each(data,function(i,arr){
				if(arr[1] == 1){
					$("#" + arr[2].substr(5)).addClass("holiday")
				}
				else{
					$("#" + arr[2].substr(5)).addClass("booked").unbind("click").html('<p><a href="bookings.php?action=view&id=' + arr[0] + '">' + arr[2].substr(8) + '</a></p>')
				}

			})
			

		})
}


// fills the month table with numbers
function fill_table(month, year, container){ 

    month_length = moment(new Date(year, month - 1)).daysInMonth();

	today = new Date(year,month - 1)
	start_day = today.getDay() + 1
	current_month = moment(today).format("MM")


	var day = 1
	var CalendarContainer = ""
	// begin the new month table
	CalendarContainer += ('<div class="calendar-month">')
	CalendarContainer += ('<h4 class="month-name">' + moment(new Date(year, month, 0)).format("MMMM") + '</h4>')
	CalendarContainer += ('<table border="0" cellspacing="0" cellpadding="0">')
	// column headings 
	CalendarContainer += ("<tr>")
	CalendarContainer += ("<th>Sun</th>")
	CalendarContainer += ("<th>Mon</th>")
	CalendarContainer += ("<th>Tue</th>")
	CalendarContainer += ("<th>Wed</th>")
	CalendarContainer += ("<th>Thu</th>")
	CalendarContainer += ("<th>Fri</th>")
	CalendarContainer += ("<th>Sat</th>")
	// pad cells before first day of month
	CalendarContainer += ("</tr><tr>")



	for (var i = 1; i < start_day; i++){
		CalendarContainer += ("<td class='no-day'></td>")
	}

	// fill the first week of days
	for (var i = start_day; i < 8; i++){
		var fetchDay = day
		if(day < 10){
			fetchDay = "0" + day
		}
		CalendarContainer +=("<td class='day' id='" + current_month + "-" + fetchDay + "'><p>" + day + "</p></td>")
		day ++
	}

	CalendarContainer += ("<tr>")

	// fill the remaining weeks
	while(day <= month_length){
		for(var i = 1 ; i <= 7 && day <= month_length; i++){
			
			var fetchDay = day
			if(day < 10){
				fetchDay = "0" + day
			}
			CalendarContainer +=("<td class='day' id='" + current_month + "-" + fetchDay + "'><p>" + day + "</p></td>")
			day ++
		}
		CalendarContainer += ("</tr><tr>")
		// the first day of the next month
		start_day = i
	}

	CalendarContainer += ("</tr></table></div>")

	container.append(CalendarContainer)

	$(".day").click(function(me){
		DayClicked(me)
	})
}