$(".calendar").ready(function(){
	LoadDefaults();
})

function LoadDefaults(){
	defaultContainer = $("#calendar")
	defaultMonth = moment().format('MM')
	defaultYear  = moment().format('YYYY')
	viewType	 = "year"

	LoadYear(defaultYear,defaultContainer);
	console.log("WebKore CMS [calendar]: Loaded Defaults");
	$("#selected-year").html(defaultYear)

	//Click hook-ins:
	$("#year-back").click(function(){
		YearSelector(0)
	})

	$("#year-forward").click(function(){
		YearSelector(1)
	})
}

function YearSelector(val){
	var currentYear = parseInt($("#selected-year").html())
	if(val == 0){
		currentYear -= 1
		$("#selected-year").html(currentYear)
		LoadYear(currentYear, defaultContainer)
	}
	else{
		currentYear += 1
		$("#selected-year").html(currentYear)
		LoadYear(currentYear, defaultContainer)

	}
}

function LoadYear(y,container){
	i = 1
	defaultContainer.html("")
	var writeToContainer = ""
	while(i < 13){
		current_month = moment(new Date(1970,i - 1,01)).format("MM")
		current_month_name = moment(new Date(1970,i - 1,01)).format("MMMM")
		writeToContainer += "<h5>" + current_month_name + "</h5>"
		writeToContainer += '<table id="month-' + current_month + '">'
		writeToContainer += '<tr><th>Day</th><th>Client</th><th class="mobile-hidden">Deposit Paid</th><th class="mobile-hidden">Total Paid</th><th>Actions</th></tr>'

		writeToContainer += '</table>'
		FetchBookingsForMonth(i,y)
		i ++
	}

	defaultContainer.html(writeToContainer)
}

function Tabledata(ghj,mobile_hidden){
	var fuck_me
	if(mobile_hidden){
		fuck_me  = '<td class="mobile-hidden">' + ghj + "</td>"
	}
	else{
		fuck_me  = "<td>" + ghj + "</td>"
	}

	return fuck_me
}

function YesNo(d){
	if(d == "1"){
		return '<i class="fa fa-check tc-tick"></i>'
	}
	else{
		return '<i class="fa fa-times tc-cross"></i>'
	}
}

function FetchBookingsForMonth(m,y){
	var APILink = "webkore_files/json_api.php"
	$.getJSON(APILink,{
			"action": "cal",
			"c-action":"load_month",
			"c-year": y,
			"c-month": m
		})
		.done(function(data){
			console.log("WebKore CMS [bookings]: Fetched Dates For " + m + "-" + y)
			$.each(data,function(i,arr){

				console.log(arr)
				var writeRow = "";
				var holiday = "";
				var buttons = "";

				if(arr[1] == "1"){
					holiday = ' class="holiday"'
					buttons = '<a href="bookings.php?holiday=RemoveHoliday&id=' + arr[0] + '" class="btn btn-holiday">Remove Holiday</a>'
				}
				else{
					buttons = '<a href="bookings.php?action=view&id=' + arr[0] + '" class="btn btn-info">View Booking</a>'

				}

				writeRow += "<tr" + holiday + ">"
				writeRow += Tabledata(moment(arr[2]).format("dddd Do"),0)
				if(arr[1] == "1"){
					writeRow += "<td></td><td></td><td></td>"
				}
				else{
					writeRow += Tabledata(arr[3],0)
					writeRow += Tabledata(YesNo(arr[4]),1)
					writeRow += Tabledata(YesNo(arr[5]),1)
				}
				writeRow += "<td>" + buttons + "</td>"

				writeRow += "</tr>"

				if(m < 10){
					MonthToAddToInTableFFS = "0" + String(m)
				}
				else{
					MonthToAddToInTableFFS = m
				}

				$("#month-" + MonthToAddToInTableFFS).append(writeRow)
				console.log("Apped Row: " + writeRow)

			})
			

		})
}