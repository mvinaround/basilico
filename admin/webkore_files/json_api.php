<?php

include("osc_core.php");
header('Content-Type: application/json');

CheckForUser();

	$action = $_GET["action"];
	$object_type = $_GET["object"];
	$object_id = $_GET["id"];

	if($action == ""){
		
		echo '["error","No Action."]';
	}

	if($action == "delete"){

		if($object_type == ""){
			echo '["error","No Type."]';
		}
		if($object_id == ""){
			echo '["error",No ID."]';
		}

		$result = SQLQuery("DELETE FROM " . $object_type . " WHERE id = " . $object_id);
		if($result == 1){
			echo '["success", 1]';
			LogAction("JSON API Call:<br> - Deleted An Object: " . $object_type);
		}
		else{
			echo '["success", 0]';
		}

	}


//Image Deletion

	if($action == "delete-image"){
		$image = htmlspecialchars_decode($_GET["file"]);


		if(unlink(dirname(__DIR__) ."/" .  $image)){
			LogAction("Deleted Image: " . $image);
			echo '["success", 1]';
			//echo ',["DEV",'. dirname(__DIR__) ."/" .  $image . ']';
		}
		else{
			echo '["success", 0],["image_url","' . $image . '"]';
		}

	}


//Calendar Date Loading

	if($action == "cal"){
		$calendar_action = $_GET["c-action"];
		$calendar_month = $_GET["c-month"];
		$calendar_year = $_GET["c-year"];

		if($calendar_action == "load_month"){
			if($calendar_month > 0){

				if($calendar_year == ""){
					$calendar_year = date("Y");
				}
				if($calendar_month < 10){
					$calendar_month = "0" . $calendar_month;
				}

				$date_from = $calendar_year . "-" . $calendar_month . "-01";
				$date_to = $calendar_year . "-" . $calendar_month . "-" . cal_days_in_month(CAL_GREGORIAN,$calendar_month,$calendar_year);

				$sql = SQLQuery("SELECT DISTINCT id, holiday, booking_date, name, deposit_paid, total_paid FROM bookings WHERE deleted IS NULL AND booking_date >= '" . $date_from . " 00:00:00' AND booking_date <='" . $date_to . " 23:59:59' ORDER BY booking_date ASC");
				
				$jsonData = array();
				while ($array = mysqli_fetch_row($sql)) {
					$jsonData[] = $array;
				}
				echo json_encode($jsonData);


			}
			else{
				echo '[0]';
			}
			
		}

		if($calendar_action == "load_year"){
			if($calendar_year > 0){

				if($calendar_year == ""){
					$calendar_year = date("Y");
				}

				$date_from = $calendar_year . "-01-01";
				$date_to = $calendar_year . "-12-31";


				$sql = SQLQuery("SELECT DISTINCT id,holiday, booking_date FROM bookings WHERE deleted IS NULL AND booking_date >= '" . $date_from . " 00:00:00' AND booking_date <='" . $date_to . " 23:59:59'");
				
				$jsonData = array();
				while ($array = mysqli_fetch_row($sql)) {
					$jsonData[] = $array;
				}
				echo json_encode($jsonData);


			}
			else{
				echo '[0]';
			}
			
		}

	}

//Feature Toggle Switches:
	
	if($action == "feature"){
		
		$golden_ticket = base64_decode($_GET["key"]);

		if($golden_ticket == $key){
			if($_GET["name"]){
				$exists = SQLQuery("SELECT name FROM features WHERE name = '" . $_GET["name"] . "'");
				if(mysqli_num_rows($exists) > 0){
					
					//Exists, update
					$result = SQLQuery("UPDATE features SET enabled = " . $_GET["val"] . " WHERE name = '" . $_GET["name"] ."'");
					if($result == 1){
						echo '["success", 1]';
					}
					else{
						echo '["success", 0]';
					}
				}
				else{

					//Create Feature variable
					$result = SQLQuery("INSERT INTO features (name,enabled) VALUES ('" . $_GET["name"] . "','" . $_GET["val"] . "') ");
					if($result == 1){
						echo '["success", 1,"Created New Option."]';
					}
					else{
						echo '["success", 0]';
					}
				}		
			}	
		}
		else{
			echo '["error","incorrect key.","' . $key . '","' . $_GET["key"] . '","' . $enc_key . '"]';
		}

	}

// Send Emails? Maybe.
if($action == "send_email"){
	echo '["error","Not Implimented."]';
}

//Dashboard Quick Stats :)

if($action == "get_quick_stats"){

	$date_from = date('Y-m-d', strtotime(date() . '-1 month'));
	$date_to = date('Y-m-d', strtotime(date() . '+1 day'));

	$unique_visitors = mysqli_num_rows(SQLQuery("SELECT DISTINCT ip FROM visits WHERE bot = 0 AND created_at >= '" . $date_from . " 00:00:00' AND created_at <='" . $date_to . " 23:59:59'"));
	$unique_views = mysqli_num_rows(SQLQuery("SELECT id FROM visits WHERE bot = 0 AND created_at >= '" . $date_from . " 00:00:00' AND created_at <='" . $date_to . " 23:59:59'"));
	$unique_pages = mysqli_num_rows(SQLQuery("SELECT DISTINCT ip,request_uri FROM visits WHERE bot = 0 AND created_at >= '" . $date_from . " 00:00:00' AND created_at <='" . $date_to . " 23:59:59'"));
	$unique_browsers = mysqli_num_rows(SQLQuery("SELECT DISTINCT user_agent FROM visits WHERE bot = 0 AND created_at >= '" . $date_from . " 00:00:00' AND created_at <='" . $date_to . " 23:59:59'"));

	echo'["success","' . $date_from . '","' . $date_to . '",' . $unique_visitors . ',' . $unique_views . ',' . $unique_pages . ',' . $unique_browsers .']';
}
