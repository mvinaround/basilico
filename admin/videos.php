<?php include("webkore_files/osc_core.php");
	LoadHeader();
	LoadMenu();

	
	$alert_box = false;
	$alert_text = "";
	$alert_type = "";

	//On adding a new one:
	if($_POST["action"] == "doadd"){
		$title = $_POST["title"];
		$embed = $_POST["embed"];
		$thumbnail_url = $_POST["thumbnail_url"];

		if(SQLQuery("INSERT INTO youtube_videos (title,embed,thumbnail_url) VALUES ('" . $title . "','" . $embed . "','" . mysqli_real_escape_string("http://img.youtube.com/vi/" . $_POST["embed"] . "/maxresdefault.jpg") . "')" )){
			$alert_box = true;
			$alert_text = "Added New Video!";
			$alert_type = "success";

			LogAction("Created a new Video: [" . $title . "," . $embed . "]");
		}
		else{
			$alert_box = true;
			$alert_text = "Failed To Save Video.";
			$alert_type = "danger";
		}

	}
	//On updating an existing one:

	elseif($_POST["action"] == "doedit"){
		if(SQLQuery("UPDATE youtube_videos SET title = '" . $_POST["title"] . "', embed = '" . $_POST["embed"] . "', thumbnail_url = '" . mysqli_real_escape_string("http://img.youtube.com/vi/" . $_POST["embed"] . "/maxresdefault.jpg") . "' WHERE id = ". $_POST["id"])){
			$alert_box = true;
			$alert_text = "Updated ". $_POST["title"] . " Video!";
			$alert_type = "success";

			LogAction("Updated Video #" . $_POST["id"] . ": [" . $_POST["title"] . "]");
		}
		else{
			$alert_box = true;
			$alert_text = "Failed To Update Video.";
			$alert_type = "danger";
		}
	}

	else{
		$action = $_GET["action"];	
	}
?>
<section>


	<h1>Youtube Videos</h1>
	<p>Currently only youtube videos are supported here... For more help on how this works, contact Jamie...</p>
	<?php
		//Alert Box:
		if($alert_box == true){
			echo '<div class="alert ' . $alert_type .'">' . $alert_text . '</div>';
		}
	?>

	<?php 
		if($action == "add" or $action == "edit"){
			$title = "";
			$embed = "";
			$id = 0;

			$send_action = "doadd";
			$button_text = "Create";

			if($action == "edit"){
				$id = $_GET["id"];
				$query = mysqli_fetch_array(SQLQuery("SELECT * FROM youtube_videos WHERE id = " . intval($id) . " LIMIT 1"));
				$title = $query["title"];
				$embed = $query["embed"];
				$thumbnail_url = $query["thumbnail_url"];

				$send_action = "doedit";
				$button_text = "Update";	

			}


	?>

	<div class="add">
		<form method="post">
			<div class="form-field">
				<label for="title">Title For Video</label>
				<input type="text" name="title" value="<?php echo $title;?>" required />
			</div>
			<div class="form-field">
				<label for="embed">Embed Code/ Video URL</label>
				<input id="embed_code" type="text" name="embed" onchange="CleanYoutubeURL()" value="<?php echo $embed;?>" required />
				<label for="embed" onclick="CleanYoutubeURL()" id="embed_status">Click here to check URL</label>	
			</div>
			<h3>Preview:</h3>
			<iframe id="yt_preview" width="560" height="315" src="" frameborder="0" allowfullscreen></iframe>
			<input type="hidden" id="yt_thumb" name="thumbnail_url" value="<?php echo $thumbnail_url ?>" />
			<input type="hidden" name="action" value="<?php echo $send_action;?>" />
			<input type="hidden" name="id" value="<?php echo $id;?>" />
			<div class="form-field">
				<input type="submit" value="<?php echo $button_text ?> Video" />
			</div>
		</form>



	</div>


	<?php
		}
		if($action == ""){

	?>
	<div class="list">
		<a href="videos.php?action=add" class="btn btn-add"><i class="fa fa-plus"></i>  Add Video</a>
		<table>
			<tr><th>Video Title</th><th>Thumbnail</th><th>Actions</th></tr>
<?php
				$result = SQLQuery("SELECT * FROM youtube_videos");

				if(mysqli_num_rows($result) > 0){

			    	while($row = mysqli_fetch_array($result)){
			    		?>
			    		<tr id="youtube_videos-<?php echo $row['id'] ?>" >
			    			<td><?php echo $row['title'] ?></td>
			    			<td>
			    			<?php 
			    				if(strlen($row['thumbnail_url']) > 0){
			    					?> <img class="list-thumbnail" src="<?php echo $row['thumbnail_url'] ?>" /> <?php
			    				}
			    				else{
			    					echo "No Thumbnail.";
			    				}

			    			?>
			    			</td>
			    			<td class="table-actions">
			    				<a class="btn btn-add" href="videos.php?action=edit&id=<?php echo $row["id"] ?>">Edit</a>
			    				<a class="btn btn-delete" onclick="DBDelete(<?php echo $row['id'] ?>,'youtube_videos')">Delete</a>

			    			</td>
			    		</tr>

			    		<?php
			   		}
			   	}
			   	else{
			    	echo "<tr><td><p>No Videos Found.</p></td></tr>";
				}
		?>
		</table>
	</div>

	<?php } ?>
</section>


<?php LoadFooter(); ?>
