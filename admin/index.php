<?php include("webkore_files/osc_core.php");
	LoadHeader();
	CheckForUserLoginScreen();

	if($_GET["action"] == "logout"){
		LogAction("User Logged Out");	
		session_destroy();
		$alert_box = true;
		$alert_type = "g-success";
		$alert_text = "Logged Out.";

	}

	$email = $_POST["email"];
	$password = $_POST["password"];

	
?>
<div id="bgp"></div>
<div class="osc-logo"></div>
<div class="login-screen">
	<h3>v<?php echo $webkore_version ?> ADMIN LOG IN</h3>
	
	<?php
		//Alert Box:
		if($alert_box == true){
			echo '<div class="alert ' . $alert_type .'">' . $alert_text . '</div>';
		}

	if($email){
		if(CheckLogin($email, $password)){
			echo '<div class="alert error">Incorrect Password/Email Address.</div>';
		}
	}

	?>
	<form method="post" action="index.php">
		<div class="form-field-glass">
			<input type="email" name="email" value="<?php echo $email; ?>" requried placeholder="Email">
		</div>
		<div class="form-field-glass">
			<input type="password" name="password" requried placeholder="Password">
			<p><a class="forgot-password-link" href="forgot-password.php?view=reset">Forgotten your password? </a></p>
		</div>
		<div class="form-field-glass">
			<input type="submit" value="Log In" requried>
		</div>
	</form>
</div>

<script src="webkore_files/lib/particles.min.js"></script>
<script>
particlesJS.load('bgp', 'webkore_files/lib/pjsconfig.json');
</script>