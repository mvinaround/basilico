<?php include("webkore_files/osc_core.php");
	LoadHeader();
	LoadMenu();

	$action = $_GET["action"];
?>
<section>


	<h1>Email Tracking</h1>
	<p>All emails sent and received within your site and system are also logged here. This is to ensure that you have a copy of any email that goes out of the system.</p>

	<?php
		if($action == "view"){
		$query = SQLQuery('SELECT * FROM emails WHERE id = ' . $_GET["id"]);
		$data = mysqli_fetch_array($query)
	?>


	<p><a href="logs-emails.php" class="btn btn-info"><i class="fa fa-angle-double-left"></i>  Back</a></p>
	<h2>Email #<?php echo $data["id"] ?> Sent On <?php echo MakeUkDate($data["date_created"]) . " At " . Get24hTime($data["date_created"]) . "  " .  FriendlyBool($data["sent"]) ?></h2>
	<p><span class="mini-title">From Email Address</span><?php echo $data["from_email"] ?></p>
	<p><span class="mini-title">From Name</span><?php echo $data["from_name"] ?></p>
	<p><span class="mini-title">To Email Address</span><?php echo $data["to_email"] ?></p>
	<p><span class="mini-title">To Name</span><?php echo $data["to_name"] ?></p>
	<p><span class="mini-title">Reply Email Address</span><?php echo $data["reply_address"] ?></p>
	<p><span class="mini-title">Email Subject</span><?php echo $data["email_subject"] ?></p>
	<p><span class="mini-title">Email Body</span><?php echo $data["email_body"] ?></p>
	<p><span class="mini-title">Email Sent?</span><?php echo FriendlyBool($data["sent"]) ?></p>


	<?php
		}
		if($action == ""){

	?>
	<div class="list">
		<table>
			<tr><th>From</th><th>To</th><th>Subject</th><th>Sent?</th><th>Sent On</th><th>Actions</th></tr>
<?php
				$result = SQLQuery("SELECT * FROM emails WHERE from_email NOT LIKE 'webkore@jjmediauk.co.uk' ");

				if(mysqli_num_rows($result) > 0){

			    	while($row = mysqli_fetch_array($result)){
			    		?>
			    		<tr>
			    			<td><?php echo $row['from_email'] ?></td>
			    			<td><?php echo $row['to_email'] ?></td>
			    			<td><?php echo $row['email_subject'] ?></td>
			    			<td>
			    				<?php echo FriendlyBool($row["sent"]) ?>
			    			</td>
			    			<td><?php echo $row['date_created'] ?></td>
			    			<td class="table-actions">
			    				<a class="btn btn-add" href="logs-emails.php?action=view&id=<?php echo $row["id"] ?>">View</a>
			    			</td>
			    		</tr>

			    		<?php
			   		}
			   	}
			   	else{
			    	echo "<tr><td><p>No Emails Found.</p></td></tr>";
				}
		?>
		</table>
	</div>

	<?php } ?>
</section>


<?php LoadFooter(); ?>
