<?php include("webkore_files/osc_core.php");
	LoadHeader();
	LoadMenu();
	CheckForUser();
	
?>
<section>
	<h1>Emails</h1>
	<br><br>
	<div class="quick-buttons">
		<a href="email-templates.php" class="dash-button">
			<i class="fa fa-newspaper-o"></i>
			<p>Email Templates</p>
		</a>

		<a href="email-clients.php" class="dash-button">
			<i class="fa fa-users"></i>
			<p>Clients</p>
		</a>

		<a href="email-sending.php" class="dash-button">
			<i class="fa fa-envelope"></i>
			<p>Send Emails</p>
		</a>

		<a href="#" class="dash-button">
			<i class="fa fa-cogs"></i>
			<p>Email Settings</p>
		</a>
				

	</div>
</section>


<?php LoadFooter(); ?>
