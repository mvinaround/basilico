<?php include("webkore_files/osc_core.php");
	LoadHeader();
	LoadMenu();
	CheckForUser();

	if($_POST["action"] == "sendqueue"){
		
		
	}
	
?>
<section>
	<h1>Sending Emails</h1>

	<h2>Email Queuing</h2>
	<p>Here you can queue emails to be sent out. This will <strong>ONLY</strong> process 1 queue at a time.<br><br>
	Please bare in mind, you have a email limit and I strongly recommend against sening more than 1 batch email a week, as this can cause legal issues with you or your company... Please take a <a target="_blank" href="https://mailchimp.com/resources/guides/how-to-avoid-spam-filters/html/">quick read up on the laws on sending batch emails</a> before sending any from this system, as I will be fairly strict about this and will disable this feature if I receive any complaints... All batch emails sent from WebKore CMS include a <strong>Unsubscribe Link</strong> to allow people to opt-out of receiving emails, this again is to comply with laws.<br><br>
	</p>

	<form action="" method="_POST" />	
		<div class="template-list">
			<h3>Choose Template:</h3>
			<div class="radio-list"/>
				<?php
					$result = SQLQuery("SELECT * FROM email_templates ");
					while($row = mysqli_fetch_array($result)){
				?>
					<div class="form-field">
						<input id="template-<?php echo base64_encode("	template-" . $row['id'])?>" type="radio" name="email-template" value="<?php echo base64_encode("	template-" . $row['id'])?>"/>
						<label for="template-<?php echo base64_encode("	template-" . $row['id'])?>"><?php echo $row['name'] ?></label>
					</div>
				<?php
					}
				?>	
			</div>
		</div>
	
		<div class="client-list">
			<h3>Choose Clients to Email:</h3>
			<div class="checkbox-list"/>
				<?php
					$result = SQLQuery("SELECT * FROM email_clients ");
					while($row = mysqli_fetch_array($result)){
				?>
					<div class="form-field">
						<input id="client-<?php echo $row['name'] ?>" type="checkbox" name="client" value="<?php echo base64_encode("	client-" . $row['id'])?>"/>
						<label for="client-<?php echo $row['name'] ?>"><?php echo $row['name'] ?></label>
					</div>
				<?php
					}
				?>	
			</div>	
	
		</div>
		

		<div class="checkbox-list"/>
		<h3>Confirm:</h3>
			<div class="form-field">
				<input type="checkbox" name="confirm" />
				<label>I confirm that I have read the above, and that I am responsible for sending the email.</label>
			</div>
		</div>

		<div class="form-field">
		<input type="submit" value="Send" id="send-button"/>
		<input type="hidden" name="action" value="sendqueue" />
		</div>
	</form>


</section>


<?php LoadFooter(); ?>
