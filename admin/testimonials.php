<?php include("webkore_files/osc_core.php");
	LoadHeader();
	LoadMenu();

	
	$alert_box = false;
	$alert_text = "";
	$alert_type = "";

	//On adding a new one:
	if($_POST["action"] == "doadd"){
		$_POST["title"] = SQLWrap($_POST["title"]);
		$_POST["from_client"] = SQLWrap($_POST["from_client"]);
		$_POST["content"] = SQLWrap($_POST["content"]);

		if(SQLQuery("INSERT INTO testimonials (title,content,from_client) VALUES ('" . $_POST["title"] . "','" . $_POST["content"] . "','" . $_POST["from_client"] . "')" )){
			$alert_box = true;
			$alert_text = "Added New Testimonial!";
			$alert_type = "success";

			LogAction("Created a new Testimonial: [" . $_POST["title"] . "]");
		}
		else{
			$alert_box = true;
			$alert_text = "Failed To Save Testimonial.";
			$alert_type = "danger";
		}

	}
	//On updating an existing one:

	elseif($_POST["action"] == "doedit"){
		$_POST["title"] = SQLWrap($_POST["title"]);
		$_POST["from_client"] = SQLWrap($_POST["from_client"]);
		$_POST["content"] = SQLWrap($_POST["content"]);

		if(SQLQuery("UPDATE testimonials SET title = '" . $_POST["title"] . "', content = '" . $_POST["content"] . "', from_client = '" . $_POST["from_client"] . "' WHERE id = ". $_POST["id"])){
			$alert_box = true;
			$alert_text = "Updated ". $_POST["title"] . " Testimonial!";
			$alert_type = "success";
			LogAction("Updated Testimonial #" . $_POST["id"] . ": [" . $_POST["title"] . "]");
		}
		else{
			$alert_box = true;
			$alert_text = "Failed To Update Snippet.";
			$alert_type = "danger";
		}
	}

	else{
		$action = $_GET["action"];	
	}
?>
<section>


	<h1>Testimonials</h1>
	<p>Display your testimonials on your site and manage them from here.</p>
	<?php
		//Alert Box:
		if($alert_box == true){
			echo '<div class="alert ' . $alert_type .'">' . $alert_text . '</div>';
		}
	?>

	<?php 
		if($action == "add" or $action == "edit"){
			$title = "";
			$content = "";
			$from_client = "";
			$send_action = "doadd";
			$id = 0;
			$button_text = "Create";

			if($action == "edit"){
				$id = $_GET["id"];
				$query = mysqli_fetch_array(SQLQuery("SELECT * FROM testimonials WHERE id = " . intval($id) . " LIMIT 1"));
				$title = $query["title"];
				$content = $query["content"];
				$from_client = $query["from_client"];
				$send_action = "doedit";
				$button_text = "Update";	

			}


	?>

	<div class="add">
		<form method="post">
			<div class="form-field">
				<label for="title">Title for Testimonial</label>
				<input type="text" name="title" value="<?php echo $title;?>" required/>
			</div>
			<div class="form-field">
				<label for="content">Their Comments</label>
				<textarea name="content" required><?php echo $content;?></textarea>
			</div>
			<div class="form-field">
				<label for="from_client">From Guest/Client</label>
				<input type="text" name="from_client" value="<?php echo $from_client;?>" required/>
			</div>

			<input type="hidden" name="action" value="<?php echo $send_action;?>" />
			<input type="hidden" name="id" value="<?php echo $id;?>" />
			<div class="form-field">
				<input type="submit" value="<?php echo $button_text ?> Testimonial" />
			</div>
		</form>
	</div>


	<?php
		}
		if($action == ""){

	?>
	<div class="list">
		<a href="testimonials.php?action=add" class="btn btn-add"><i class="fa fa-plus"></i>  Create New Testimonial</a>
		<table>
			<tr><th>Testimonial Title</th><th>From</th><th>Actions</th></tr>
<?php
				$result = SQLQuery("SELECT * FROM testimonials");

				if(mysqli_num_rows($result) > 0){

			    	while($row = mysqli_fetch_array($result)){
			    		?>
			    		<tr id="testimonials-<?php echo $row['id'] ?>" >
			    			<td><?php echo $row['title'] ?></td>
			    			<td><?php echo $row['from_client'] ?></td>
			    			<td class="table-actions">
			    				<a class="btn btn-add" href="testimonials.php?action=edit&id=<?php echo $row["id"] ?>">Edit</a>
			    				<a class="btn btn-delete" onclick="DBDelete(<?php echo $row['id'] ?>,'testimonials')">Delete</a>

			    			</td>
			    		</tr>

			    		<?php
			   		}
			   	}
			   	else{
			    	echo "<tr><td><p>No Testimonials Found.</p></td></tr>";
				}
		?>
		</table>
	</div>

	<?php } ?>
</section>


<?php LoadFooter(); ?>
