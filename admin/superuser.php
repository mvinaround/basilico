<?php 
	include("webkore_files/osc_core.php");
	LoadHeader();
	LoadMenu();
	CheckForUser();
	CheckForAdmin();
	if($_GET["clear"] == "emails"){
		SQLQuery("DELETE FROM emails WHERE 1");
		LogAction("Superuser Cleared All Emails In Log.");
	}

	if($_GET["clear"] == "user-logs"){
		SQLQuery("DELETE FROM user_logs WHERE 1");
		LogAction("Superuser Cleared The User Log.");
	}
	if($_GET["clear"] == "visits"){
		SQLQuery("DELETE FROM visits WHERE 1");
		LogAction("Superuser Cleared The Site Performance Visitor Data.");
	}
	if($_GET["clear"] == "prk"){
		SQLQuery("DELETE FROM password_reset_key WHERE 1");
		LogAction("Superuser Cleared The Site Performance Visitor Data.");
	}

	if($_POST["action"] == "update_facebook_access_token"){
		UpdateSetting("fb_access_token");

	}
	
?>
<section style="background-color: rgba(255,255,255,0.8) !important;">
	<h1>Dashboard</h1>
	<p>Welcome Back Jamie</p>
	<br>
	<h2>WebKore Facebook Access Token:</h2>
	<form for="superuser.php" method="POST">
		<?php FormField("FB Access Token","text",1,"get-setting","nigga");?>

		<div class="form-field">
						<input type="hidden" name="action" value="update_facebook_access_token" />
						<input type="submit" value="Update Facebook Access Token" />	
					</div>
		
	</form>
	<h2>Clear All Emails In Database</h2>
	<p>This CANNOT be undone!</p>
	<a href="#" onclick="$('#clear-email-button').show()">SHOW ME THE BUTTON!</a>
	<br>
	<p class="hide-button" id="clear-email-button"><a href="superuser.php?clear=emails" class="btn btn-delete"><i class="fa fa-times"></i>  CLEAR</a></p>
	<br><br>

	<h2>Clear The User Log</h2>
	<p>This CANNOT be undone!</p>
	<a href="#" onclick="$('#clear-user-log').show()">SHOW ME THE BUTTON!</a>
	<br>
	<p class="hide-button" id="clear-user-log"><a href="superuser.php?clear=user-logs" class="btn btn-delete"><i class="fa fa-times"></i>  CLEAR</a></p>
	<br><br>

	<h2>Clear The Site Performance</h2>
	<p>This CANNOT be undone!</p>
	<a href="#" onclick="$('#clear-visits').show()">SHOW ME THE BUTTON!</a>
	<br>
	<p class="hide-button" id="clear-visits"><a href="superuser.php?clear=visits" class="btn btn-delete"><i class="fa fa-times"></i>  CLEAR</a></p>
	<br><br>

	<h2>Clear Password Reset Keys</h2>
	<p>This CANNOT be undone!</p>
	<a href="#" onclick="$('#clear-prk').show()">SHOW ME THE BUTTON!</a>
	<br>
	<p class="hide-button" id="clear-prk"><a href="superuser.php?clear=prk" class="btn btn-delete"><i class="fa fa-times"></i>  CLEAR</a></p>
	<br><br>	

	<h2>Features Enabled</h2>
	<br>
	
	<h3 class="option-header">Booking Management:</h3>
	<div class="onoffswitch">
		<input type="checkbox"  class="onoffswitch-checkbox" id="f_bookings" <?php echo GetFeature("checked","","bookings") ?> >
		<label class="onoffswitch-label" for="f_bookings"></label>
	</div>

	<h3 class="option-header">Site Performance Tracking:</h3>
	<div class="onoffswitch">
		<input type="checkbox"  class="onoffswitch-checkbox" id="f_site-performance" <?php echo GetFeature("checked","","site-performance") ?> >
		<label class="onoffswitch-label" for="f_site-performance"></label>
	</div>

	<h3 class="option-header">Gallery:</h3>
	<div class="onoffswitch">
		<input type="checkbox"  class="onoffswitch-checkbox" id="f_gallery" <?php echo GetFeature("checked","","gallery") ?> >
		<label class="onoffswitch-label" for="f_gallery"></label>
	</div>

	<h3 class="option-header">Testimonials:</h3>
	<div class="onoffswitch">
		<input type="checkbox"  class="onoffswitch-checkbox" id="f_testimonials" <?php echo GetFeature("checked","","testimonials") ?> >
		<label class="onoffswitch-label" for="f_testimonials"></label>
	</div>

	<h3 class="option-header">Banners:</h3>
	<div class="onoffswitch">
		<input type="checkbox"  class="onoffswitch-checkbox" id="f_banners" <?php echo GetFeature("checked","","banners") ?> >
		<label class="onoffswitch-label" for="f_banners"></label>
	</div>

	<h3 class="option-header">Youtube Videos:</h3>
	<div class="onoffswitch">
		<input type="checkbox"  class="onoffswitch-checkbox" id="f_videos" <?php echo GetFeature("checked","","videos") ?> >
		<label class="onoffswitch-label" for="f_videos"></label>
	</div>

	<h3 class="option-header">Email System:</h3>
	<div class="onoffswitch">
		<input type="checkbox"  class="onoffswitch-checkbox" id="f_emails" <?php echo GetFeature("checked","","emails") ?> >
		<label class="onoffswitch-label" for="f_emails"></label>
	</div>

	<h3 class="option-header">Services:</h3>
	<div class="onoffswitch">
		<input type="checkbox"  class="onoffswitch-checkbox" id="f_services" <?php echo GetFeature("checked","","services") ?> >
		<label class="onoffswitch-label" for="f_services"></label>
	</div>

	<h3 class="option-header">Inventory:</h3>
	<div class="onoffswitch">
		<input type="checkbox"  class="onoffswitch-checkbox" id="f_inventory" <?php echo GetFeature("checked","","inventory") ?> >
		<label class="onoffswitch-label" for="f_inventory"></label>
	</div>

	<h3 class="option-header">Dynamic Menus:</h3>
	<div class="onoffswitch">
		<input type="checkbox"  class="onoffswitch-checkbox" id="f_dynamic-menus" <?php echo GetFeature("checked","","dynamic-menus") ?> >
		<label class="onoffswitch-label" for="f_dynamic-menus"></label>
	</div>

	<h3 class="option-header">Social Media Admin:</h3>
	<div class="onoffswitch">
		<input type="checkbox"  class="onoffswitch-checkbox" id="f_social-media" <?php echo GetFeature("checked","","social-media") ?> >
		<label class="onoffswitch-label" for="f_social-media"></label>
	</div>


	<br><br>
	<h2>Active Password Reset Keys</h2>
	<div class="list">
		<table>
			<tr><th>ID</th><th>Email</th><th>Key</th><th>Timestamp</th></tr>
<?php
				$result = SQLQuery("SELECT * FROM password_reset_key ORDER BY email_sent DESC");

				if(mysqli_num_rows($result) > 0){

			    	while($row = mysqli_fetch_array($result)){
			    		?>
			    		<tr>
			    			<td><?php echo $row['id'] ?></td>
			    			<td><?php echo $row['email'] ?></td>
			    			<td><?php echo $row['reset_key'] ?></td>
			    			<td><?php echo $row['email_sent'] ?></td>
			    		</tr>

			    		<?php
			   		}
			   	}
			   	else{
			    	echo "<tr><td><p>No Logs Found.</p></td></tr>";
				}
		?>
		</table>
	</div>
	<h2>Failed Login Attempts</h2>
	<div class="list">
		<table>
			<tr><th>ID</th><th>IP</th><th>Query</th><th>Referer</th><th>Email</th><th>Password</th><th>Timestamp</th></tr>
<?php
				$result = SQLQuery("SELECT * FROM login_attempts ORDER BY date_created DESC");

				if(mysqli_num_rows($result) > 0){

			    	while($row = mysqli_fetch_array($result)){
			    		?>
			    		<tr>
			    			<td><?php echo $row['id'] ?></td>
			    			<td><?php echo $row['ip'] ?></td>
			    			<td><?php echo $row['query'] ?></td>
			    			<td><?php echo $row['referer'] ?></td>
			    			<td><?php echo $row['email'] ?></td>
			    			<td><?php echo $row['password'] ?></td>			    			
			    			<td><?php echo $row['date_created'] ?></td>
			    		</tr>

			    		<?php
			   		}
			   	}
			   	else{
			    	echo "<tr><td><p>No Logs Found.</p></td></tr>";
				}
		?>
		</table>
	</div>

	<h2>Error list</h2>
	<div class="list">
		<table>
			<tr><th>IP</th><th>Query</th><th>Referer URL</th><th>Browser Type</th><th>Remote Host</th><th>Requested Page</th><th>Hostname / Location</th><th>Region</th><th>Location Lat:Long</th><th>Postcode</th><th>Visited</th></tr>
			<?php

				$data = SQLQuery("SELECT * FROM visits WHERE NOT request_uri LIKE '%robots%' AND NOT request_uri LIKE '%favicon%' AND query_string LIKE '%error%' OR query_string LIKE '%author%' ORDER BY created_at DESC");
				if(mysqli_num_rows($data) > 0){

			    	while($row = mysqli_fetch_array($data)){
			    		?>
			    		<tr>
			    			<td><?php echo $row['ip'] ?></td>
			    			<td><?php echo $row['query_string'] ?></td>
			    			<td><?php echo $row['referer'] ?></td>
			    			<td><?php echo $row['user_agent'] ?></td>
			    			<td><?php echo $row['remote_host'] ?></td>
			    			<td><?php echo $row['request_uri'] ?></td>
			    			<td><?php echo $row['hostname'] ?></td>
			    			<td><?php echo $row['region'] ?></td>
			    			<td><?php echo $row['location'] ?></td>
			    			<td><?php echo $row['postcode'] ?></td>
			    			<td><?php echo $row['created_at'] ?></td>
			    		</tr>

			    		<?php
			   		}

			   		mysqli_data_seek($data, 0);
			   	}
			   	else{
			    	echo "<tr><td><p>No Visits Found.</p></td></tr>";
				}
			?>
		</table>
	</div>
</section>
<script>
NyanNyanNyan("cat");
var APIKey = "<?php echo $enc_key ?>"

$(".onoffswitch-checkbox").change(function() {
	var val = 0;
	var name = this.id.replace("f_","")

    if(this.checked) {
    	val = 1
    }

    FeatureToggle(name,val,APIKey);
});


</script>


<?php LoadFooter(); ?>
