<?php include("webkore_files/osc_core.php");
	LoadHeader();
	LoadMenu();

	
	$alert_box = false;
	$alert_text = "";
	$alert_type = "";

	//On adding a new one:
	if($_POST["action"] == "doadd"){
		$name = $_POST["name"];
		$email = $_POST["email"];

		$check = SQLQuery("SELECT * FROM email_clients WHERE email = '" . $email . "'");

		if(mysqli_num_rows($check) > 0){

			$row = mysqli_fetch_array($check);

			$alert_box = true;
			$alert_text = "Failed To Save Client. The Client Already Exists: <strong> #". $row["id"] ." [". $row["name"] . " - " . $row["email"] . "]</strong>" ;
			$alert_type = "warning";
		}

		else{
			if(SQLQuery("INSERT INTO email_clients (name,email) VALUES ('" . $name . "','" . strtolower($email) . "')" )){
				$alert_box = true;
				$alert_text = "Added Client!";
				$alert_type = "success";
	
				LogAction("Created a new Client: [" . $name . "]");
			}
			else{
				$alert_box = true;
				$alert_text = "Failed To Save Client.";
				$alert_type = "danger";
			}
		}

	}

	else{
		$action = $_GET["action"];	
	}
?>
<section>


	<h1>Email Clients</h1>
	<p>Email Clients are listed here. These are the people you can choose to send a batch email to. If you have added one incorrectly please delete it and add it again. There will eventually be a quick way to add clients onto this list...</p>
	<?php
		//Alert Box:
		if($alert_box == true){
			echo '<div class="alert ' . $alert_type .'">' . $alert_text . '</div>';
		}
	?>

	<?php 
		if($action == "add"){
	?>

	<div class="add">
		<form method="post">
			<div class="form-field">
				<label for="name">Name</label>
				<input type="text" name="name" required/>
			</div>
			<div class="form-field">
				<label for="email">Email</label>
				<input type="text" name="email" required/>
			</div>
			<input type="hidden" name="action" value="doadd" />
			<input type="hidden" name="id" value="0" />
			<div class="form-field">
				<input type="submit" value="Create Client" />
			</div>
		</form>
	</div>


	<?php
		}
		if($action == ""){

	?>
	<div class="list">
		<a href="email-clients.php?action=add" class="btn btn-add"><i class="fa fa-plus"></i>  Create New Client</a>
		<table>
			<tr><th>UID</th><th>Name</th><th>Email Address</th><th>Actions</th></tr>
<?php
				$result = SQLQuery("SELECT * FROM email_clients");

				if(mysqli_num_rows($result) > 0){

			    	while($row = mysqli_fetch_array($result)){
			    		?>
			    		<tr id="email_clients-<?php echo $row['id'] ?>" >
			    			<td><?php echo $row['id'] ?></td>
			    			<td><?php echo $row['name'] ?></td>
			    			<td><?php echo $row['email'] ?></td>
			    			<td class="table-actions">
			    				<a class="btn btn-delete" onclick="DBDelete(<?php echo $row['id'] ?>,'email_clients')">Delete</a>

			    			</td>
			    		</tr>

			    		<?php
			   		}
			   	}
			   	else{
			    	echo "<tr><td><p>No Clients Found.</p></td></tr>";
				}
		?>
		</table>
	</div>

	<?php } ?>
</section>


<?php LoadFooter(); ?>
