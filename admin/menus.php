<?php
	include("webkore_files/osc_core.php");
	LoadHeader();
	LoadMenu();

	
	$alert_box = false;
	$alert_text = "";
	$alert_type = "";

	$name = $_POST["name"];
	$link = str_replace("%2F","/", urlencode($_POST["link"]));
	$visible = SQLCheckbox($_POST["visible"]);
	$id = $_POST["id"];

	//On adding a new one:
	if($_POST["action"] == "doadd"){
		

		if(SQLQuery("INSERT INTO menu_items (name,link,visible) VALUES ('" . $name . "','" . $link . "','" . $visible  . "')" )){
			$alert_box = true;
			$alert_text = "Added New Menu Item!";
			$alert_type = "success";

			LogAction("Created a new Menu Item: [" . $name . " -> " . $link . " ]");
		}
		else{
			$alert_box = true;
			$alert_text = "Failed To Save Menu item.";
			$alert_type = "danger";
		}

	}
	//On updating an existing one:

	elseif($_POST["action"] == "doedit"){
		if(SQLQuery("UPDATE menu_items SET name = '" . $name . "', link = '" . $link . "', visible = '" . $visible  . "' WHERE id = ". $id)){
			$alert_box = true;
			$alert_text = "Updated ". $_POST["name"] . " Menu item!";
			$alert_type = "success";

			LogAction("Updated Menu Item #" . $_POST["id"] . ": [" . $name . " -> " . $link . " ]");
		}
		else{
			$alert_box = true;
			$alert_text = "Failed To Update Menu Item.";
			$alert_type = "danger";
		}
	}

	else{
		$action = $_GET["action"];	
	}
?>

<section>


	<h1>Dynamic Menu Items</h1>
	<p>Links shown in the main menu on your site. Any local pages eg your services etc, link to "services" or "services.php". Any problems please get in touch.</p>
	<?php
		//Alert Box:
		if($alert_box == true){
			echo '<div class="alert ' . $alert_type .'">' . $alert_text . '</div>';
		}
	?>

	<?php 
		if($action == "add" or $action == "edit"){
			$name = "";
			$link = "";
			$visible = "";
			$send_action = "doadd";
			$id = 0;
			$button_text = "Create";

			if($action == "edit"){
				$id = $_GET["id"];
				$query = mysqli_fetch_array(SQLQuery("SELECT * FROM menu_items WHERE id = " . intval($id) . " LIMIT 1"));
				$name = $query["name"];
				$link = urldecode($query["link"]);
				$visible = $query["visible"];
				$send_action = "doedit";
				$button_text = "Update";	

			}


	?>

	<div class="add">
		<form method="post">
			<?php 
				FormField("Name","text",1,$name,"Contact Us");
				FormField("Link","text",1,$link,"HTTP Link or site extention eg http://google.com/ or contact.php");
				FormField("Visible","checkbox",0,$visible,"Show in menu?");



			?>
			<input type="hidden" name="action" value="<?php echo $send_action;?>" />
			<input type="hidden" name="id" value="<?php echo $id;?>" />
			<div class="form-field">
				<input type="submit" value="<?php echo $button_text ?> Menu Item" />
			</div>
		</form>
	</div>


	<?php
		}
		if($action == ""){

	?>
	<div class="list">
		<a href="menus.php?action=add" class="btn btn-add"><i class="fa fa-plus"></i>  Create New Menu Item</a>
		<table>
			<tr><th>Name</th><th>Link</th><th>Visible</th><th>Actions</th></tr>
<?php
				$result = SQLQuery("SELECT * FROM menu_items");

				if(mysqli_num_rows($result) > 0){

			    	while($row = mysqli_fetch_array($result)){
			    		?>
			    		<tr id="menu_items-<?php echo $row['id'] ?>" >
			    			<td><?php echo $row['name'] ?></td>
			    			<td><a href="<?php echo $row['link'] ?>" ></a><?php echo $row['link'] ?></td>
			    			<td><?php TickCross($row["visible"]);?></td>
			    			<td class="table-actions">
			    				<a class="btn btn-add" href="menus.php?action=edit&id=<?php echo $row["id"] ?>">Edit</a>
			    				<a class="btn btn-delete" onclick="DBDelete(<?php echo $row['id'] ?>,'menu_items')">Delete</a>

			    			</td>
			    		</tr>

			    		<?php
			   		}
			   	}
			   	else{
			    	echo "<tr><td><p>No Menu Items Found.</p></td></tr>";
				}
		?>
		</table>
	</div>

	<?php } ?>
</section>


<?php LoadFooter(); ?>
