<?php require("files/php/header.php") ?>
<?php require("files/php/menu-otr.php") ?>
<section class="content">
		<?php echo GetSnippet("On the Road - Top") ?>
		<object id="menu" data='files/van-menu.pdf'
        type='application/pdf'
        width='100%'
        height='100%'></object>
		<?php echo GetSnippet("On the Road - Bottom") ?>
</section>

<div class="fotos">
	<div class="image" style="background-image: url(files/img/van1.jpg)">
	</div>
	<div class="image" style="background-image: url(files/img/van2.jpg)"></div>
	<div class="image" style="background-image: url(files/img/van3.jpg)"></div>
</div>


<?php require("files/php/footer.php") ?>
